package com.newgfmisinterface.rpinterfaceebidding.service;

import com.newgfmisinterface.rpinterfaceebidding.dao.EbiddingItemDao;
import com.newgfmisinterface.rpinterfaceebidding.dto.EbiddingCheckDto;
import com.newgfmisinterface.rpinterfaceebidding.dto.EbiddingFooterItemDto;
import com.newgfmisinterface.rpinterfaceebidding.dto.EbiddingHeaderItemDto;
import com.newgfmisinterface.rpinterfaceebidding.dto.EbiddingItemDto;
import com.newgfmisinterface.rpinterfaceebidding.validate.TextEbiddingValidate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Service
public class CheckErrorService {
    private static Logger LOGGER = LoggerFactory.getLogger(CheckErrorService.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private EbiddingItemDao ebiddingItemDao;

    private TextEbiddingValidate textEbiddingValidate = new TextEbiddingValidate();

    private Executor executor = Executors.newFixedThreadPool(10);

    public CompletableFuture<EbiddingCheckDto> checkProcess(String textPasser,String ebiddingDocType,int row, EbiddingHeaderItemDto headerLine, EbiddingFooterItemDto endLine) {
System.out.println(textPasser);
        return CompletableFuture.supplyAsync(() -> {
            List<Integer> errorList = new ArrayList<>();

            EbiddingCheckDto ebiddingCheckDto = new EbiddingCheckDto();

            ebiddingCheckDto.setNumber(row);
            ebiddingCheckDto.setHaveError(false);

            EbiddingItemDto linedataObj = textEbiddingValidate.splitlinetoObj(textPasser);

            ebiddingCheckDto.setErrorList(errorList);

            Double money;
            try {
                money = Double.parseDouble(linedataObj.getAmount());
            } catch (NumberFormatException ex) {
                money = (double) 0;
            } catch (NullPointerException ex) {
                money = (double) 0;
            }
            try {
            // check error Excel Number 04
            if (textEbiddingValidate.isMoneyLessThanZero(money)) {
                ebiddingCheckDto.setHaveError(true);
                errorList.add(4);
            }
            // check error Excel Number 06
            if (!textEbiddingValidate.validateFileDate(linedataObj.getTransdate(), headerLine, endLine)) {
                ebiddingCheckDto.setHaveError(true);
                errorList.add(6);
            }
            // check error Excel Number 07
            if (!textEbiddingValidate.validateFileDocType(linedataObj.getDoctype(), headerLine, endLine)) {
                ebiddingCheckDto.setHaveError(true);
                errorList.add(7);
            }
            // check error Excel Number 08
            if (!textEbiddingValidate.filenameAndTypefileStateIncome(ebiddingDocType, headerLine, endLine)) {
                ebiddingCheckDto.setHaveError(true);
                errorList.add(8);
            }
            // check error Excel Number 09
            if (textEbiddingValidate.compareDepartment(linedataObj.getCct(), linedataObj.getCctowner())) {
                ebiddingCheckDto.setHaveError(true);
                errorList.add(9);
            }
            // check error Excel Number 10
            if (linedataObj.getDoctype().equals("R3") || linedataObj.getDoctype().equals("R4")) {
                if (!textEbiddingValidate.validateDigit(linedataObj.getXref3(), 16)) {
                    ebiddingCheckDto.setHaveError(true);
                    errorList.add(10);
                }
            }
            // check error Excel Number 11
            if (linedataObj.getDoctype().equals("R3") || linedataObj.getDoctype().equals("RC")) {
                if (!textEbiddingValidate.costCenterType(linedataObj.getZzdeposit(), linedataObj.getZzowner())) {
                    ebiddingCheckDto.setHaveError(true);
                    errorList.add(11);
                }
            }
            // check error Excel Number 13
            if (!textEbiddingValidate.validateTypeDocR4(linedataObj.getDoctype(), linedataObj.getCctowner(), linedataObj.getZzowner())) {
                ebiddingCheckDto.setHaveError(true);
                errorList.add(13);
            }
            // check error Excel Number 14
            if (linedataObj.getDoctype().equals("RD") || linedataObj.getDoctype().equals("R4")) {
                if (textEbiddingValidate.costCenterType(linedataObj.getZzdeposit(), linedataObj.getZzowner())) {
                    ebiddingCheckDto.setHaveError(true);
                    errorList.add(14);
                }
            }
            // check error Excel Number 15
            if (linedataObj.getDoctype().equals("RC") || linedataObj.getDoctype().equals("RD")) {
                if (!textEbiddingValidate.referenceKeyAndType(linedataObj.getXref3())) {
                    ebiddingCheckDto.setHaveError(true);
                    errorList.add(15);
                }
            }
            // check error Excel Number 16
            if ((linedataObj.getCct() == "") || (linedataObj.getCct() == null)) {
                ebiddingCheckDto.setHaveError(true);
                errorList.add(16);
            }
            // check error Excel Number 17
            int checkPaymentCenterCol4 = ebiddingItemDao.checkPaymentCenter(linedataObj.getCct());
            int checkPaymentCenterCol5 = ebiddingItemDao.checkPaymentCenter(linedataObj.getCctowner());
            if ((checkPaymentCenterCol4 >= 1) && (checkPaymentCenterCol5 >= 1)) {
                ebiddingCheckDto.setHaveError(true);
                errorList.add(17);
            }
            // check error Excel Number 18
            int costCenterCol4 = ebiddingItemDao.countCostcenter(linedataObj.getCct());
            int costCenterCol5 = ebiddingItemDao.countCostcenter(linedataObj.getCctowner());
            if ((costCenterCol4 >= 1) && (costCenterCol5 >= 1)) {
                ebiddingCheckDto.setHaveError(true);
                errorList.add(18);
            }
            // check error Excel Number 20
            if (!textEbiddingValidate.countCadepositaccount(linedataObj.getZzdeposit(), linedataObj.getZzowner())) {
                int countCadepositAccount7 = ebiddingItemDao.countCadepositAccount(linedataObj.getZzdeposit());
                int countPaymentCenter8 = ebiddingItemDao.countPaymentCenter(linedataObj.getZzowner());
                if ((countCadepositAccount7 >= 1) && (countPaymentCenter8 >= 1)) {
                    ebiddingCheckDto.setHaveError(true);
                    errorList.add(20);
                }
            }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                ebiddingCheckDto.setEbiddingItemDto(linedataObj);
                ebiddingCheckDto.setErrorList(errorList);
                return ebiddingCheckDto;
            }
        }, executor).exceptionally(ex -> {
            return null;
        });
    }
}
