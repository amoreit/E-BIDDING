package com.newgfmisinterface.rpinterfaceebidding.service;

import com.newgfmisinterface.rpinterfaceebidding.dto.EbiddingErrorDto;
import com.newgfmisinterface.rpinterfaceebidding.dto.EbiddingItemDto;
import org.springframework.stereotype.Service;

@Service
public class ErrorInsertService {
    public static EbiddingErrorDto errorPasser(EbiddingItemDto dataObj,String filename, String dateString, String docType, Integer errorNo) {

        EbiddingErrorDto ebiddingErrorDto = new EbiddingErrorDto();

        ebiddingErrorDto.setFilename(filename);
        ebiddingErrorDto.setTransdate(dateString);
        ebiddingErrorDto.setItemno(dataObj.getItemno());
        ebiddingErrorDto.setDoctype(docType);
        ebiddingErrorDto.setErrorno(errorNo);

        // error desc
        if (errorNo == 1) {
            ebiddingErrorDto.setMsgid("EBD-000001");
            ebiddingErrorDto.setMessage("รายการได้ถูกบันทึกแล้ว");
        } else if (errorNo == 3) {
            ebiddingErrorDto.setMsgid("EBD-000003");
            ebiddingErrorDto.setMessage("ไม่พบข้อมูลในไฟล์");
        } else if (errorNo == 4) {
            ebiddingErrorDto.setMsgid("EBD-000004");
            ebiddingErrorDto.setMessage("จำนวนเงินในบรรทัดรายการจะต้องไม่เท่ากับศูนย์");
        } else if (errorNo == 5) {
            ebiddingErrorDto.setMsgid("EBD-000005");
            ebiddingErrorDto.setMessage("จำนวนเงินรวมไม่เท่ากับจำนวนเงินของบรรทัดรายการรวมกัน");
        } else if (errorNo == 6) {
            ebiddingErrorDto.setMsgid("EBD-000006");
            ebiddingErrorDto.setMessage("วันที่ผ่านรายการแตกต่างกับวันที่ของชื่อไฟล์");
        } else if (errorNo == 7) {
            ebiddingErrorDto.setMsgid("EBD-000007");
            ebiddingErrorDto.setMessage("ไม่พบประเภทเอกสารตามที่กำหนด");
        } else if (errorNo == 8) {
            ebiddingErrorDto.setMsgid("EBD-000008");
            ebiddingErrorDto.setMessage("ประเภทเอกสารไม่สัมพันธ์กัน");
        } else if (errorNo == 9) {
            ebiddingErrorDto.setMsgid("EBD-000009");
            ebiddingErrorDto.setMessage("การจัดเก็บและนำส่งเงินรายได้แผ่นดิน ไม่ต้องระบุเจ้าของบัญชีเงินฝากคลังและรหัสบัญชีเงินฝากคลัง");
        } else if (errorNo == 10) {
            ebiddingErrorDto.setMsgid("EBD-000010");
            ebiddingErrorDto.setMessage("คีย์อ้างอิง 16 หลักไม่ถูกต้อง");
        } else if (errorNo == 11) {
            ebiddingErrorDto.setMsgid("EBD-000011");
            ebiddingErrorDto.setMessage("คการจัดเก็บและนำส่งเงินรายได้แผ่นดิน ไม่ต้องระบุเจ้าของบัญชีเงินฝากคลังและรหัสบัญชีเงินฝากคลัง");
        } else if (errorNo == 12) {
            ebiddingErrorDto.setMsgid("EBD-000012");
            ebiddingErrorDto.setMessage("ยอดเงินรวมของประเภทเอกสารไม่ตรงกับยอดเงินนำฝาก ใน Statement");
        } else if (errorNo == 13) {
            ebiddingErrorDto.setMsgid("EBD-000013");
            ebiddingErrorDto.setMessage("การจัดเก็บและนำส่งเงินนอกงบแทนกัน ศูนย์ต้นทุนเจ้าของรายได้กับเจ้าของเงินฝากคลัง จะต้องอยู่ในหน่วยงานเดียวกัน");
        } else if (errorNo == 14) {
            ebiddingErrorDto.setMsgid("EBD-000014");
            ebiddingErrorDto.setMessage("การจัดเก็บและนำส่งเงินนอกงบประมาณ จะต้องระบุเจ้าของบัญชีเงินฝากคลังและรหัสบัญชีเงินฝากคลัง");
        } else if (errorNo == 15) {
            ebiddingErrorDto.setMsgid("EBD-000015");
            ebiddingErrorDto.setMessage("ประเภทเอกสารจัดเก็บไม่ต้องระบุคีย์อ้างอิง 16 หลัก");
        } else if (errorNo == 16) {
            ebiddingErrorDto.setMsgid("EBD-000016");
            ebiddingErrorDto.setMessage("กรุณาระบุศูนย์ต้นทุน");
        } else if (errorNo == 17) {
            ebiddingErrorDto.setMsgid("EBD-000017");
            ebiddingErrorDto.setMessage("รหัสหน่วยเบิกจ่ายไม่สามารถใช้งานได้");
        } else if (errorNo == 18) {
            ebiddingErrorDto.setMsgid("EBD-000018");
            ebiddingErrorDto.setMessage("ศูนย์ต้นทุนถูกบล็อค");
        } else if (errorNo == 20) {
            ebiddingErrorDto.setMsgid("EBD-000020");
            ebiddingErrorDto.setMessage("ไม่พบรหัสบัญชีเงินฝากคลังและเจ้าของบัญชีเงินฝากคลังในระบบ");
        } else {
            ebiddingErrorDto.setMsgid("EBD-000000");
            ebiddingErrorDto.setMessage("ไม่พบข้อมูลในไฟล์");
        }
        return ebiddingErrorDto;
    }
}
