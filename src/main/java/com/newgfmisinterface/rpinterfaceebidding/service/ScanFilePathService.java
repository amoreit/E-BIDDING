package com.newgfmisinterface.rpinterfaceebidding.service;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import com.newgfmisinterface.rpinterfaceebidding.validate.FileNameFormatValidate;

@Service
public class ScanFilePathService {
    private FileNameFormatValidate fileNameFormateEbiddingValidate = new FileNameFormatValidate();

    public synchronized List<String> listFilesForFolder(File folder, String projectName) {

        ArrayList<String> result = new ArrayList<>();

        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry, projectName);
            } else {

                String filename = fileEntry.getName();

                if (!fileNameFormateEbiddingValidate.isFileProjectCode(filename, projectName)) {
                    continue;
                }

                result.add(filename);
            }
        }

        return result;
    }
    public synchronized List<String> listConvertAndPostFilesForFolder(File folder, String projectName) {

        ArrayList<String> result = new ArrayList<>();
        System.out.println("<<< folder.listFiles(): " + folder.listFiles());
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listConvertAndPostFilesForFolder(fileEntry, projectName);
            } else {

                String filename = fileEntry.getName();

                if (!fileNameFormateEbiddingValidate.isConvertAndPostFileProjectCode(filename, projectName)) {
                    continue;
                }

                result.add(filename);
            }
        }

        return result;
    }
    public synchronized List<String> listFilesOutputForFolder(File folder, String fileName){
        ArrayList<String> result = new ArrayList<>();

        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesOutputForFolder(fileEntry, fileName);
            } else {

                String filename = fileEntry.getName();

                if (!fileNameFormateEbiddingValidate.isFileNameOutput(filename, fileName)) {
                    continue;
                }

                result.add(filename);
            }
        }

        return result;
    }
    public synchronized List<String> listFilesForProcess(File folder, String projectName) {

        ArrayList<String> result = new ArrayList<>();

        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForProcess(fileEntry, projectName);
            } else {

                String filename = fileEntry.getName();

                if (!fileNameFormateEbiddingValidate.isFileProject(filename, projectName)) {
                    continue;
                }

                result.add(filename);
            }
        }

        return result;
    }
}
