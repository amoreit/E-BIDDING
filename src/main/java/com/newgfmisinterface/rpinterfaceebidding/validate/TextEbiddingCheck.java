package com.newgfmisinterface.rpinterfaceebidding.validate;

import java.text.SimpleDateFormat;
import java.util.*;
import com.newgfmisinterface.rpinterfaceebidding.dao.EbiddingErrorDao;
import com.newgfmisinterface.rpinterfaceebidding.dto.EbiddingErrorDto;
import com.newgfmisinterface.rpinterfaceebidding.service.FileStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.newgfmisinterface.rpinterfaceebidding.dao.EbiddingLogDao;
import com.newgfmisinterface.rpinterfaceebidding.dto.EbiddingLogDto;

@Component
public class TextEbiddingCheck {
    private static Logger LOGGER = LoggerFactory.getLogger(TextEbiddingCheck.class);
    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private EbiddingLogDao ebiddingLogDao;

    @Autowired
    private EbiddingErrorDao ebiddingErrorDao;

    private TextEbiddingValidate textEbiddingValidate = new TextEbiddingValidate();

    public void validateFileNull(String filename) {

        String doctype1 = filename.substring(9, 11);
        String doctype2 = filename.substring(11, 13);
        String date = filename.substring(13, 21);
        String newDateString = textEbiddingValidate.dateFormat(date);

        String[] docTypeFile = {doctype1,doctype2};
        int countList = docTypeFile.length;
        int countLog = 0;
        for (String docType : docTypeFile) {
            EbiddingLogDto saveLog = new EbiddingLogDto();
            String dateTimeStart = new SimpleDateFormat("dd-MMM-yy HH:mm:ss").format(new Date());
            saveLog.setFilename(filename);
            saveLog.setTransdate(newDateString);
            saveLog.setDoctype(docType);
            saveLog.setStatus("E");
            saveLog.setTotamt(0);
            saveLog.setTotrec(0);
            saveLog.setPostrec(0);
            saveLog.setErrrec(0);
            saveLog.setUploadname("");
            saveLog.setUploadstart(dateTimeStart);
            saveLog.setUploadfinish("");
            try {
                ebiddingLogDao.addLog(saveLog);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
                // check error Excel Number 03
                EbiddingErrorDto EBD000003Error = new EbiddingErrorDto();
                EBD000003Error.setFilename(filename);
                EBD000003Error.setTransdate(newDateString);
                EBD000003Error.setDoctype(docType);
                EBD000003Error.setItemno(0);
                EBD000003Error.setMsgno("EBD-000003");
                EBD000003Error.setMsgid("EBD-000003");
                EBD000003Error.setMessage("ไม่พบข้อมูลในไฟล์");
                EBD000003Error.setUploaddate(newDateString);
                try {
                    ebiddingErrorDao.add(EBD000003Error);
                    fileStorageService.fileOutPass(filename, date, docType, "Error");
                    fileStorageService.textFileError(EBD000003Error, date);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            countLog = countLog + 1;
            if(countLog == countList){
                fileStorageService.deleteFileToProcessDir(filename);
            }
        }
    }
}
