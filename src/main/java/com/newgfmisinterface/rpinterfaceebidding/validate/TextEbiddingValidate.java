package com.newgfmisinterface.rpinterfaceebidding.validate;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.newgfmisinterface.rpinterfaceebidding.dto.EbiddingHeaderItemDto;
import com.newgfmisinterface.rpinterfaceebidding.dto.EbiddingItemDto;
import com.newgfmisinterface.rpinterfaceebidding.dto.EbiddingFooterItemDto;
import com.newgfmisinterface.rpinterfaceebidding.dto.EbiddingStatementDto;

public class TextEbiddingValidate {

    public TextEbiddingValidate() {
    }

    public static String dateFormat(String Date) {
        final String OLD_FORMAT = "yyyyMMdd";
        final String NEW_FORMAT = "dd-MMM-yy";

        String oldDateString = Date;
        String newDateString;
        SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
        try {
            Date d = sdf.parse(oldDateString);
            sdf.applyPattern(NEW_FORMAT);
            newDateString = sdf.format(d);
        } catch (Exception e) {
            e.printStackTrace();
            newDateString = "";
        }
        return newDateString;
    }
    public static String dateFormatFileText(String Date){
        final String OLD_FORMAT = "yyyy-MM-dd";
        final String NEW_FORMAT = "dd-MMM-yy";

        String oldDateString = Date;
        String newDateString;
        SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
        try {
            Date d = sdf.parse(oldDateString);
            sdf.applyPattern(NEW_FORMAT);
            newDateString = sdf.format(d);
        } catch(Exception e) {
            newDateString = "";
        }
        return newDateString;
    }
    public boolean filenameAndTypefileStateIncome(String docTypeFile, EbiddingHeaderItemDto headerLine, EbiddingFooterItemDto endLine) {

        String HeaderDocType = headerLine.getTypeFile();
        String FooterDocType = endLine.getTypeFile();

        if (!docTypeFile.equals(HeaderDocType) || !docTypeFile.equals(FooterDocType)) {
            return false;
        } else {
            return true;
        }
    }
    public boolean checkFileNameStatement(String fileName, String fileDate) {
        String date = fileName.substring(13, 20);
        if (!date.equals(fileDate)) {
            return false;
        } else {
            return true;
        }
    }
    public boolean checkFileNameStatementNetAmount(EbiddingStatementDto data) {
        Double totalAmountOriginal = new Double(0);
        Double amount01 = new Double(0);
        Double amount02 = new Double(0);
        Double amount03 = new Double(0);
        Double amount04 = new Double(0);
        Double amount = new Double(0);
        try {
            totalAmountOriginal = Double.parseDouble(data.getTotalamt());
            amount01 = Double.parseDouble(data.getAmount01());
            amount02 = Double.parseDouble(data.getAmount02());
            amount03 = Double.parseDouble(data.getAmount03());
            amount04 = Double.parseDouble(data.getAmount04());
            amount = (amount01+amount02+amount03+amount04);
        } catch (NumberFormatException ex) {
            totalAmountOriginal = new Double(0);
            return false;
        }
        DecimalFormat decimalFormat = new DecimalFormat("0.000");
        String totalAmount = decimalFormat.format(totalAmountOriginal);
        String sumAmount = decimalFormat.format(amount);
        if (totalAmount.equals(sumAmount)) {
            return true;
        } else {
            return false;
        }
    }
    public boolean compareDepartment(String costCenter4, String costCenter5) {

        String costCenterCol4 = costCenter4.substring(0, 5);
        String costCenterCol5 = costCenter5.substring(0, 5);

        if (costCenterCol4.equals(costCenterCol5)) {
            if (costCenterCol4.equals("03004") && costCenterCol5.equals("03004")) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    public boolean validateDigit(String data, Integer number) {
        String check = data == null ? "" : data.trim();
        String checkData = check == null ? "" : check;
        Integer count = checkData.length();

        Integer amount = count == null ? 0 : count;
        if (!amount.equals(number)) {
            return false;
        }
        return true;
    }

    public boolean validateTypeDocR4(String dataType, String dataCol5, String dataCol8) {
        String ceckDataType = dataType;
        String dataEquals5 = dataCol5;
        String dataEquals8 = dataCol8;
        switch (ceckDataType) {
            case "R4":
                if (!dataEquals5.equals(dataEquals8)) {
                    return false;
                } else {
                    return true;
                }
            default:
                return true;
        }
    }

    public boolean costCenterType(String dataCol7, String dataCol8) {
        String checkCol7 = dataCol7 == null ? "" : dataCol7.trim();
        String checkData7 = checkCol7 == null ? "" : checkCol7;

        String checkCol8 = dataCol8 == null ? "" : dataCol8.trim();
        String checkData8 = checkCol8 == null ? "" : checkCol8;
        if (checkData7.equals("") && checkData8.equals("")) {
            return true;
        }
        return false;
    }

    public boolean referenceKeyAndType(String dataCol) {
        String check = dataCol == null ? "" : dataCol.trim();
        String checkData = check == null ? "" : check;
        Integer digitData = 0;
        Integer count = 0;
        for (int i = 0; i < checkData.length(); i++) {
            count++;
        }
        Integer amount = count == null ? 0 : count;
        if (amount.equals(digitData)) {
            return true;
        }
        return false;
    }

    public boolean countCadepositaccount(String dataCol7, String dataCol8) {
        if (!dataCol7.trim().equals("") && !dataCol8.trim().equals("")) {
            return false;
        }
        return true;
    }

    public boolean isMoneyLessThanZero(Double money) {
        if ((money <= 0) || (money <= 0.0)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean validateTotalRecord(Integer totalRecord, EbiddingItemDto endLinedata) {

        Integer totalRecReal = 0;

        try {
            totalRecReal = Integer.parseInt(endLinedata.getCct());
        } catch (NumberFormatException ex) {
        }

        if (totalRecord.equals(totalRecReal)) {
            return true;
        } else {
            return false;
        }

    }

    public boolean validateTotalAmount(Double totalAmount, EbiddingFooterItemDto endLinedata) {

        Double totalAmountOriginal = new Double(0);
        try {
            totalAmountOriginal = Double.parseDouble(endLinedata.getTotalAmount());
        } catch (NumberFormatException ex) {
            totalAmountOriginal = new Double(0);
            return false;
        }

        DecimalFormat decimalFormat = new DecimalFormat("0.000");
        String firstNumberAsString = decimalFormat.format(totalAmount);
        String secondNumberAsString = decimalFormat.format(totalAmountOriginal);

//        System.out.println(firstNumberAsString);
//        System.out.println(secondNumberAsString);

        if (firstNumberAsString.equals(secondNumberAsString)) {
            return true;
        } else {
            return false;
        }

    }

    public boolean validateFileDate(String date, EbiddingHeaderItemDto headerLine, EbiddingFooterItemDto endLine) {
        String HeaderDate = headerLine.getDateFile();
        String FooterDate = endLine.getDateFile();

        if (!date.equals(HeaderDate) || !date.equals(FooterDate)) {
            return false;
        } else {
            return true;
        }
    }

    public boolean validateFileDocType(String docType, EbiddingHeaderItemDto headerLine, EbiddingFooterItemDto endLine) {
        String HeaderDate = headerLine.getTypeFile();
        String FooterDate = endLine.getTypeFile();

        if (!docType.equals(HeaderDate) || !docType.equals(FooterDate)) {
            return false;
        } else {
            return true;
        }
    }

    public EbiddingItemDto splitlinetoObj(String textline) {

        String[] lineParse = textline.split("\\|");

        Integer numCol = lineParse.length;

        EbiddingItemDto ebiddingInterface = new EbiddingItemDto();
        if (numCol > 0) {
            if (!("".equals(lineParse[0]))) {
                try {
                    Integer Itemno = Integer.parseInt(lineParse[0]);
                    ebiddingInterface.setItemno(Itemno);

                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        }

        if (numCol > 1) {
            if (!("".equals(lineParse[1]))) {
                ebiddingInterface.setTransdate(lineParse[1].trim());
            }
        }

        if (numCol > 2) {
            if (!("".equals(lineParse[2]))) {
                ebiddingInterface.setDoctype(lineParse[2].trim());
            }
        }

        if (numCol > 3) {
            if (!("".equals(lineParse[3]))) {
                ebiddingInterface.setCct(lineParse[3].trim());
            }
        }

        if (numCol > 4) {
            if (!("".equals(lineParse[4]))) {
                ebiddingInterface.setCctowner(lineParse[4].trim());
            }
        }
        if (numCol > 5) {
            if (!("".equals(lineParse[5]))) {
                ebiddingInterface.setXref3(lineParse[5].trim());
            }
        }
        if (numCol > 6) {
            if (!("".equals(lineParse[6]))) {
                ebiddingInterface.setZzdeposit(lineParse[6].trim());
            }
        }
        if (numCol > 7) {
            if (!("".equals(lineParse[7]))) {
                ebiddingInterface.setZzowner(lineParse[7].trim());
            }
        }
        if (numCol > 8) {
            if (!("".equals(lineParse[8]))) {
                ebiddingInterface.setAmount(lineParse[8].trim());
            }
        }

        return ebiddingInterface;

    }

    public EbiddingHeaderItemDto splitLineHeader(String textLine) {

        String[] lineParse = textLine.split("\\|");

        Integer numCol = lineParse.length;

        EbiddingHeaderItemDto ebiddingHeaderItemDto = new EbiddingHeaderItemDto();
        if (numCol > 0) {
            if (!("".equals(lineParse[0]))) {
                ebiddingHeaderItemDto.setHeaderFile(lineParse[0].trim());
            }
        }
        if (numCol > 1) {
            if (!("".equals(lineParse[1]))) {
                ebiddingHeaderItemDto.setTypeFile(lineParse[1].trim());
            }
        }
        if (numCol > 2) {
            if (!("".equals(lineParse[2]))) {
                ebiddingHeaderItemDto.setDateFile(lineParse[2].trim());
            }
        }

        return ebiddingHeaderItemDto;

    }

    public EbiddingFooterItemDto splitLineFooter(String textLine) {

        String[] lineParse = textLine.split("\\|");

        Integer numCol = lineParse.length;

        EbiddingFooterItemDto ebiddingFooterItemDto = new EbiddingFooterItemDto();
        if (numCol > 0) {
            if (!("".equals(lineParse[0]))) {
                ebiddingFooterItemDto.setFooterFile(lineParse[0].trim());
            }
        }
        if (numCol > 1) {
            if (!("".equals(lineParse[1]))) {
                ebiddingFooterItemDto.setTypeFile(lineParse[1].trim());
            }
        }
        if (numCol > 2) {
            if (!("".equals(lineParse[2]))) {
                ebiddingFooterItemDto.setTotalRecord(lineParse[2].trim());
            }
        }

        if (numCol > 3) {
            if (!("".equals(lineParse[3]))) {
                ebiddingFooterItemDto.setTotalAmount(lineParse[3].trim());
            }
        }

        if (numCol > 4) {
            if (!("".equals(lineParse[4]))) {
                ebiddingFooterItemDto.setDateFile(lineParse[4].trim());
            }
        }

        return ebiddingFooterItemDto;

    }

    public EbiddingStatementDto splitlinetoObjStatement(String textline) {

        String[] lineParse = textline.split("\\|");

        Integer numCol = lineParse.length;

        EbiddingStatementDto ebiddingStatementDto = new EbiddingStatementDto();

        if (numCol > 0) {
            if (!("".equals(lineParse[0]))) {
                ebiddingStatementDto.setTransdate(lineParse[0].trim());
            }
        }
        if (numCol > 1) {
            if (!("".equals(lineParse[1]))) {
                ebiddingStatementDto.setTotalamt(lineParse[1].trim());
            }
        }

        if (numCol > 2) {
            if (!("".equals(lineParse[2]))) {
                ebiddingStatementDto.setAmount01(lineParse[2].trim());
            }
        }

        if (numCol > 3) {
            if (!("".equals(lineParse[3]))) {
                ebiddingStatementDto.setAmount02(lineParse[3].trim());
            }
        }

        if (numCol > 4) {
            if (!("".equals(lineParse[4]))) {
                ebiddingStatementDto.setAmount03(lineParse[4].trim());
            }
        }
        if (numCol > 5) {
            if (!("".equals(lineParse[5]))) {
                ebiddingStatementDto.setAmount04(lineParse[5].trim());
            }
        }
        if (numCol > 6) {
            if (!("".equals(lineParse[6]))) {
                ebiddingStatementDto.setAmount05(lineParse[6].trim());
            }
        }

        return ebiddingStatementDto;

    }
}
