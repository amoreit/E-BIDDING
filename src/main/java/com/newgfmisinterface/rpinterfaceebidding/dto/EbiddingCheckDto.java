package com.newgfmisinterface.rpinterfaceebidding.dto;

import java.util.ArrayList;
import java.util.List;
public class EbiddingCheckDto {
    private int number;
    private boolean haveError;
    private EbiddingItemDto ebiddingItemDto;
    private List<Integer> errorList = new ArrayList<Integer>();

    public EbiddingCheckDto() {
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public boolean isHaveError() {
        return haveError;
    }

    public void setHaveError(boolean haveError) {
        this.haveError = haveError;
    }

    public EbiddingItemDto getEbiddingItemDto() {
        return ebiddingItemDto;
    }

    public void setEbiddingItemDto(EbiddingItemDto ebiddingItemDto) {
        this.ebiddingItemDto = ebiddingItemDto;
    }

    public List<Integer> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<Integer> errorList) {
        this.errorList = errorList;
    }

    @Override
    public String toString() {
        return "EbiddingCheckDto{" +
                "number=" + number +
                ", haveError=" + haveError +
                ", ebiddingItemDto=" + ebiddingItemDto +
                ", errorList=" + errorList +
                '}';
    }
}
