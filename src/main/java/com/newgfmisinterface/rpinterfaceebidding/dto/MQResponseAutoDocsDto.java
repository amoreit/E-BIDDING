package com.newgfmisinterface.rpinterfaceebidding.dto;

import javax.xml.bind.annotation.XmlElement;

public class MQResponseAutoDocsDto {
    private MQResponseAutoDocDto autoDoc;

    public MQResponseAutoDocsDto() {
        super();
    }
    public MQResponseAutoDocsDto(MQResponseAutoDocDto autoDoc) {
        super();
        this.autoDoc = autoDoc;
    }


    public MQResponseAutoDocDto getAutoDoc() {
        return autoDoc;
    }
    @XmlElement(name = "AUTODOC")
    public void setAutoDoc(MQResponseAutoDocDto autoDoc) {
        this.autoDoc = autoDoc;
    }

    @Override
    public String toString() {
        return "MQResponseAutoDocsDto{" +
                "autoDoc=" + autoDoc +
                '}';
    }
}
