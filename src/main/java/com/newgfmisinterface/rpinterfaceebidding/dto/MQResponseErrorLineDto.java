package com.newgfmisinterface.rpinterfaceebidding.dto;

import javax.xml.bind.annotation.XmlElement;

public class MQResponseErrorLineDto {
    private String type = "";
    private String code = "";
    private String text = "";
    private String line = "";


    public MQResponseErrorLineDto() {
        super();
    }
    public MQResponseErrorLineDto(String type, String code, String text, String line) {
        super();
        this.type = type;
        this.code = code;
        this.text = text;
        this.line = line;
    }
    public String getType() {
        return type;
    }
    @XmlElement(name = "TYPE")
    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }
    @XmlElement(name = "CODE")
    public void setCode(String code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }
    @XmlElement(name = "TEXT")
    public void setText(String text) {
        this.text = text;
    }

    public String getLine() {
        return line;
    }
    @XmlElement(name = "LINE")
    public void setLine(String line) {
        this.line = line;
    }

    @Override
    public String toString() {
        return "MQResponseErrorLineDto{" +
                "type='" + type + '\'' +
                ", code='" + code + '\'' +
                ", text='" + text + '\'' +
                ", line='" + line + '\'' +
                '}';
    }
}
