package com.newgfmisinterface.rpinterfaceebidding.dto;

import javax.xml.bind.annotation.XmlElement;

public class MQRequestHeaderDto {
    private String userwebonline = "";
    private String docType = "";
    private String compCode = "";
    private String dateDoc = "";
    private String datePost = "";
    private String period = "";
    private String currency = "";
    private String paymentCenter = "";
    private String brDocNo = "";
    private String poDocNo = "";
    private String invDocNo = "";
    private String revInvDocNo = "";
    private String accDocNo = "";
    private String revAccDocNo = "";
    private String fiscalYear = "";
    private String revFiscalYear = "";
    private String paymentMethod = "";
    private String costCenter1 = "";
    private String costCenter2 = "";
    private String hdreference = "";
    private String docHeaderText = "";
    private String headerDesc = "";
    private String hdRef2 = "";
    private String revDatePost = "";
    private String revReasonPost = "";
    private String previousDocNo = "";
    private String hdRefLog = "";

    public MQRequestHeaderDto() {
        super();
    }
    public MQRequestHeaderDto(String userwebonline, String docType, String compCode, String dateDoc, String datePost,
                              String period, String currency, String paymentCenter, String brDocNo, String poDocNo,
                              String invDocNo, String revInvDocNo, String accDocNo, String revAccDocNo, String fiscalYear,
                              String revFiscalYear, String paymentMethod, String costCenter1, String costCenter2, String hdreference,
                              String docHeaderText, String headerDesc, String hdRef2, String revDatePost, String revReasonPost,
                              String previousDocNo, String hdRefLog) {
        super();
        this.userwebonline = userwebonline;
        this.docType = docType;
        this.compCode = compCode;
        this.dateDoc = dateDoc;
        this.datePost = datePost;
        this.period = period;
        this.currency = currency;
        this.paymentCenter = paymentCenter;
        this.brDocNo = brDocNo;
        this.poDocNo = poDocNo;
        this.invDocNo = invDocNo;
        this.revInvDocNo = revInvDocNo;
        this.accDocNo = accDocNo;
        this.revAccDocNo = revAccDocNo;
        this.fiscalYear = fiscalYear;
        this.revFiscalYear = revFiscalYear;
        this.paymentMethod = paymentMethod;
        this.costCenter1 = costCenter1;
        this.costCenter2 = costCenter2;
        this.hdreference = hdreference;
        this.docHeaderText = docHeaderText;
        this.headerDesc = headerDesc;
        this.hdRef2 = hdRef2;
        this.revDatePost = revDatePost;
        this.revReasonPost = revReasonPost;
        this.previousDocNo = previousDocNo;
        this.hdRefLog = hdRefLog;
    }
    public String getDocType() {
        return docType;
    }
    @XmlElement(name = "DOC_TYPE")
    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getCompCode() {
        return compCode;
    }
    @XmlElement(name = "COMP_CODE")
    public void setCompCode(String compCode) {
        this.compCode = compCode;
    }

    public String getUserwebonline() {
        return userwebonline;
    }
    @XmlElement(name = "USERWEBONLINE")
    public void setUserwebonline(String userwebonline) {
        this.userwebonline = userwebonline;
    }

    public String getDateDoc() {
        return dateDoc;
    }
    @XmlElement(name = "DATE_DOC")
    public void setDateDoc(String dateDoc) {
        this.dateDoc = dateDoc;
    }

    public String getDatePost() {
        return datePost;
    }
    @XmlElement(name = "DATE_POST")
    public void setDatePost(String datePost) {
        this.datePost = datePost;
    }

    public String getCurrency() {
        return currency;
    }
    @XmlElement(name = "CURRENCY")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCostCenter1() {
        return costCenter1;
    }
    @XmlElement(name = "COST_CENTER1")
    public void setCostCenter1(String costCenter1) {
        this.costCenter1 = costCenter1;
    }

    public String getCostCenter2() {
        return costCenter2;
    }
    @XmlElement(name = "COST_CENTER2")
    public void setCostCenter2(String costCenter2) {
        this.costCenter2 = costCenter2;
    }

    public String getPeriod() {
        return period;
    }
    @XmlElement(name = "PERIOD")
    public void setPeriod(String period) {
        this.period = period;
    }

    public String getDocHeaderText() {
        return docHeaderText;
    }
    @XmlElement(name = "DOC_HEADER_TEXT")
    public void setDocHeaderText(String docHeaderText) {
        this.docHeaderText = docHeaderText;
    }

    public String getPaymentCenter() {
        return paymentCenter;
    }
    @XmlElement(name = "PAYMENT_CENTER")
    public void setPaymentCenter(String paymentCenter) {
        this.paymentCenter = paymentCenter;
    }

    public String getBrDocNo() {
        return brDocNo;
    }
    @XmlElement(name = "BR_DOC_NO")
    public void setBrDocNo(String brDocNo) {
        this.brDocNo = brDocNo;
    }

    public String getPoDocNo() {
        return poDocNo;
    }
    @XmlElement(name = "PO_DOC_NO")
    public void setPoDocNo(String poDocNo) {
        this.poDocNo = poDocNo;
    }

    public String getInvDocNo() {
        return invDocNo;
    }
    @XmlElement(name = "INV_DOC_NO")
    public void setInvDocNo(String invDocNo) {
        this.invDocNo = invDocNo;
    }

    public String getRevInvDocNo() {
        return revInvDocNo;
    }
    @XmlElement(name = "REV_INV_DOC_NO")
    public void setRevInvDocNo(String revInvDocNo) {
        this.revInvDocNo = revInvDocNo;
    }

    public String getAccDocNo() {
        return accDocNo;
    }
    @XmlElement(name = "ACC_DOC_NO")
    public void setAccDocNo(String accDocNo) {
        this.accDocNo = accDocNo;
    }

    public String getRevAccDocNo() {
        return revAccDocNo;
    }
    @XmlElement(name = "REV_ACC_DOC_NO")
    public void setRevAccDocNo(String revAccDocNo) {
        this.revAccDocNo = revAccDocNo;
    }

    public String getFiscalYear() {
        return fiscalYear;
    }
    @XmlElement(name = "FISCAL_YEAR")
    public void setFiscalYear(String fiscalYear) {
        this.fiscalYear = fiscalYear;
    }

    public String getRevFiscalYear() {
        return revFiscalYear;
    }
    @XmlElement(name = "REV_FISCAL_YEAR")
    public void setRevFiscalYear(String revFiscalYear) {
        this.revFiscalYear = revFiscalYear;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }
    @XmlElement(name = "PAYMENT_METHOD")
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getHdreference() {
        return hdreference;
    }
    @XmlElement(name = "HDREFERENCE")
    public void setHdreference(String hdreference) {
        this.hdreference = hdreference;
    }

    public String getHeaderDesc() {
        return headerDesc;
    }
    @XmlElement(name = "HEADER_DESC")
    public void setHeaderDesc(String headerDesc) {
        this.headerDesc = headerDesc;
    }

    public String getHdRef2() {
        return hdRef2;
    }
    @XmlElement(name = "HD_REF2")
    public void setHdRef2(String hdRef2) {
        this.hdRef2 = hdRef2;
    }

    public String getRevDatePost() {
        return revDatePost;
    }
    @XmlElement(name = "REV_DATE_POST")
    public void setRevDatePost(String revDatePost) {
        this.revDatePost = revDatePost;
    }

    public String getRevReasonPost() {
        return revReasonPost;
    }
    @XmlElement(name = "REV_REASON_POST")
    public void setRevReasonPost(String revReasonPost) {
        this.revReasonPost = revReasonPost;
    }

    public String getPreviousDocNo() {
        return previousDocNo;
    }
    @XmlElement(name = "PREVIOUS_DOC_NO")
    public void setPreviousDocNo(String previousDocNo) {
        this.previousDocNo = previousDocNo;
    }

    public String getHdRefLog() {
        return hdRefLog;
    }
    @XmlElement(name = "HD_REF_LOG")
    public void setHdRefLog(String hdRefLog) {
        this.hdRefLog = hdRefLog;
    }

    @Override
    public String toString() {
        return "MQRequestHeaderDto{" +
                "userwebonline='" + userwebonline + '\'' +
                ", docType='" + docType + '\'' +
                ", compCode='" + compCode + '\'' +
                ", dateDoc='" + dateDoc + '\'' +
                ", datePost='" + datePost + '\'' +
                ", period='" + period + '\'' +
                ", currency='" + currency + '\'' +
                ", paymentCenter='" + paymentCenter + '\'' +
                ", brDocNo='" + brDocNo + '\'' +
                ", poDocNo='" + poDocNo + '\'' +
                ", invDocNo='" + invDocNo + '\'' +
                ", revInvDocNo='" + revInvDocNo + '\'' +
                ", accDocNo='" + accDocNo + '\'' +
                ", revAccDocNo='" + revAccDocNo + '\'' +
                ", fiscalYear='" + fiscalYear + '\'' +
                ", revFiscalYear='" + revFiscalYear + '\'' +
                ", paymentMethod='" + paymentMethod + '\'' +
                ", costCenter1='" + costCenter1 + '\'' +
                ", costCenter2='" + costCenter2 + '\'' +
                ", hdreference='" + hdreference + '\'' +
                ", docHeaderText='" + docHeaderText + '\'' +
                ", headerDesc='" + headerDesc + '\'' +
                ", hdRef2='" + hdRef2 + '\'' +
                ", revDatePost='" + revDatePost + '\'' +
                ", revReasonPost='" + revReasonPost + '\'' +
                ", previousDocNo='" + previousDocNo + '\'' +
                ", hdRefLog='" + hdRefLog + '\'' +
                '}';
    }
}
