package com.newgfmisinterface.rpinterfaceebidding.dto;

import javax.xml.bind.annotation.XmlElement;
public class XmlDataReply {
    private String filename;
    private String transdate;
    private String doctype;
    private String itemno;
    private String docno;

    public XmlDataReply() {
        super();
    }
    public XmlDataReply(String filename, String transdate, String doctype,String itemno, String docno) {
        super();
        this.filename = filename;
        this.transdate = transdate;
        this.doctype = doctype;
        this.itemno = itemno;
        this.docno = docno;
    }
    public String getFilename() {return filename;}
    @XmlElement(name = "FILE_NAME")
    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getTransdate() {
        return transdate;
    }
    @XmlElement(name = "TRANS_DATE")
    public void setTransdate(String transdate) {
        this.transdate = transdate;
    }

    public String getDoctype() {
        return doctype;
    }
    @XmlElement(name = "DOC_TYPE")
    public void setDoctype(String doctype) {
        this.doctype = doctype;
    }

    public String getItemno() {
        return itemno;
    }
    @XmlElement(name = "ITEMNO")
    public void setItemno(String itemno) {
        this.itemno = itemno;
    }
    public String getDocno() {
        return docno;
    }
    @XmlElement(name = "DOC_NO")
    public void setDocno(String docno) {
        this.docno = docno;
    }


    @Override
    public String toString() {
        return "XmlDataReply{" +
                "filename='" + filename + '\'' +
                ", transdate='" + transdate + '\'' +
                ", doctype='" + doctype + '\'' +
                ", itemno='" + itemno + '\'' +
                ", docno='" + docno + '\'' +
                '}';
    }
}
