package com.newgfmisinterface.rpinterfaceebidding.dto;

import javax.xml.bind.annotation.XmlElement;

public class MQRequestFromDocDto {
    private String hdreference = "";
    private String docType = "";
    private String compCode = "";
    private String accDocNo = "";
    private String fiscalYear = "";
    private String formId = "";

    public MQRequestFromDocDto() {
        super();
    }
    public MQRequestFromDocDto(String hdreference, String docType, String compCode, String accDocNo, String fiscalYear, String formId) {
        super();
        this.hdreference = hdreference;
        this.docType = docType;
        this.compCode = compCode;
        this.accDocNo = accDocNo;
        this.fiscalYear = fiscalYear;
        this.formId = formId;
    }
    public String getHdreference() {
        return hdreference;
    }
    @XmlElement(name = "HDREFERENCE")
    public void setHdreference(String hdreference) {
        this.hdreference = hdreference;
    }

    public String getDocType() {
        return docType;
    }
    @XmlElement(name = "DOC_TYPE")
    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getCompCode() {
        return compCode;
    }
    @XmlElement(name = "COMP_CODE")
    public void setCompCode(String compCode) {
        this.compCode = compCode;
    }

    public String getAccDocNo() {
        return accDocNo;
    }
    @XmlElement(name = "ACC_DOC_NO")
    public void setAccDocNo(String accDocNo) {
        this.accDocNo = accDocNo;
    }

    public String getFiscalYear() {
        return fiscalYear;
    }
    @XmlElement(name = "FISCAL_YEAR")
    public void setFiscalYear(String fiscalYear) {
        this.fiscalYear = fiscalYear;
    }

    public String getFormId() {
        return formId;
    }
    @XmlElement(name = "FORMID")
    public void setFormId(String formId) {
        this.formId = formId;
    }

    @Override
    public String toString() {
        return "MQRequestFromDocDto{" +
                "hdreference='" + hdreference + '\'' +
                ", docType='" + docType + '\'' +
                ", compCode='" + compCode + '\'' +
                ", accDocNo='" + accDocNo + '\'' +
                ", fiscalYear='" + fiscalYear + '\'' +
                ", formId='" + formId + '\'' +
                '}';
    }
}
