package com.newgfmisinterface.rpinterfaceebidding.dto;

import javax.xml.bind.annotation.XmlElement;

public class XmlData {

    private String filename;
    private String transdate;
    private String doctype;
    private String itemno;
    private String cct;
    private String cctowner;
    private String xref3;
    private String zzdeposit;
    private String zzowner;
    private String amount;

    public XmlData() {
        super();
    }
    public XmlData(String filename, String transdate, String doctype,String itemno, String cct, String cctowner,String xref3, String zzdeposit, String zzowner,String amount) {
        super();
        this.filename = filename;
        this.transdate = transdate;
        this.doctype = doctype;
        this.itemno = itemno;
        this.cct = cct;
        this.cctowner = cctowner;
        this.xref3 = xref3;
        this.zzdeposit = zzdeposit;
        this.zzowner = zzowner;
        this.amount = amount;
    }

    public String getFilename() {return filename;}
    @XmlElement(name = "FILE_NAME")
    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getTransdate() {
        return transdate;
    }
    @XmlElement(name = "TRANS_DATE")
    public void setTransdate(String transdate) {
        this.transdate = transdate;
    }

    public String getDoctype() {
        return doctype;
    }
    @XmlElement(name = "DOC_TYPE")
    public void setDoctype(String doctype) {
        this.doctype = doctype;
    }

    public String getItemno() {
        return itemno;
    }
    @XmlElement(name = "ITEMNO")
    public void setItemno(String itemno) {
        this.itemno = itemno;
    }

    public String getCct() {
        return cct;
    }
    @XmlElement(name = "CCT")
    public void setCct(String cct) {
        this.cct = cct;
    }

    public String getCctowner() {
        return cctowner;
    }
    @XmlElement(name = "CCT_OWNER")
    public void setCctowner(String cctowner) {
        this.cctowner = cctowner;
    }

    public String getXref3() {
        return xref3;
    }
    @XmlElement(name = "XREF3")
    public void setXref3(String xref3) {
        this.xref3 = xref3;
    }

    public String getZzdeposit() {
        return zzdeposit;
    }
    @XmlElement(name = "ZZDEPOSIT")
    public void setZzdeposit(String zzdeposit) {
        this.zzdeposit = zzdeposit;
    }

    public String getZzowner() {
        return zzowner;
    }
    @XmlElement(name = "ZZOWNER")
    public void setZzowner(String zzowner) {
        this.zzowner = zzowner;
    }

    public String getAmount() {
        return amount;
    }
    @XmlElement(name = "AMOUNT")
    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "XmlData{" +
                "filename='" + filename + '\'' +
                ", transdate='" + transdate + '\'' +
                ", doctype='" + doctype + '\'' +
                ", itemno='" + itemno + '\'' +
                ", cct='" + cct + '\'' +
                ", cctowner='" + cctowner + '\'' +
                ", xref3='" + xref3 + '\'' +
                ", zzdeposit='" + zzdeposit + '\'' +
                ", zzowner='" + zzowner + '\'' +
                ", amount='" + amount + '\'' +
                '}';
    }
}
