package com.newgfmisinterface.rpinterfaceebidding.dto;

public class MqLogDto {
    private String sendseq;
    private String senddate;
    private Integer ebiddingno;
    private Integer ebiddingseq;
    private String ebiddingfilename;
    private String docno;
    private String postdate;
    private Integer sendloop;
    private String sendstatus;
    private String issend;
    private String createddate;
    private String createduser;
    private String CREATEDPAGE;
    private String ipaddr;
    private String kostl;
    private String updateddate;
    private String updatedpage;
    private String updateduser;
    private String recordstatus;
    private String isactive;

    public String getSendseq() {
        return sendseq;
    }

    public void setSendseq(String sendseq) {
        this.sendseq = sendseq;
    }

    public String getSenddate() {
        return senddate;
    }

    public void setSenddate(String senddate) {
        this.senddate = senddate;
    }

    public Integer getEbiddingno() {
        return ebiddingno;
    }

    public void setEbiddingno(Integer ebiddingno) {
        this.ebiddingno = ebiddingno;
    }

    public Integer getEbiddingseq() {
        return ebiddingseq;
    }

    public void setEbiddingseq(Integer ebiddingseq) {
        this.ebiddingseq = ebiddingseq;
    }

    public String getEbiddingfilename() {
        return ebiddingfilename;
    }

    public void setEbiddingfilename(String ebiddingfilename) {
        this.ebiddingfilename = ebiddingfilename;
    }

    public String getDocno() {
        return docno;
    }

    public void setDocno(String docno) {
        this.docno = docno;
    }

    public String getPostdate() {
        return postdate;
    }

    public void setPostdate(String postdate) {
        this.postdate = postdate;
    }

    public Integer getSendloop() {
        return sendloop;
    }

    public void setSendloop(Integer sendloop) {
        this.sendloop = sendloop;
    }

    public String getSendstatus() {
        return sendstatus;
    }

    public void setSendstatus(String sendstatus) {
        this.sendstatus = sendstatus;
    }

    public String getIssend() {
        return issend;
    }

    public void setIssend(String issend) {
        this.issend = issend;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }

    public String getCreateduser() {
        return createduser;
    }

    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

    public String getCREATEDPAGE() {
        return CREATEDPAGE;
    }

    public void setCREATEDPAGE(String CREATEDPAGE) {
        this.CREATEDPAGE = CREATEDPAGE;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getKostl() {
        return kostl;
    }

    public void setKostl(String kostl) {
        this.kostl = kostl;
    }

    public String getUpdateddate() {
        return updateddate;
    }

    public void setUpdateddate(String updateddate) {
        this.updateddate = updateddate;
    }

    public String getUpdatedpage() {
        return updatedpage;
    }

    public void setUpdatedpage(String updatedpage) {
        this.updatedpage = updatedpage;
    }

    public String getUpdateduser() {
        return updateduser;
    }

    public void setUpdateduser(String updateduser) {
        this.updateduser = updateduser;
    }

    public String getRecordstatus() {
        return recordstatus;
    }

    public void setRecordstatus(String recordstatus) {
        this.recordstatus = recordstatus;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    @Override
    public String toString() {
        return "MqLogDto{" +
                "sendseq='" + sendseq + '\'' +
                ", senddate='" + senddate + '\'' +
                ", ebiddingno=" + ebiddingno +
                ", ebiddingseq=" + ebiddingseq +
                ", ebiddingfilename='" + ebiddingfilename + '\'' +
                ", docno='" + docno + '\'' +
                ", postdate='" + postdate + '\'' +
                ", sendloop=" + sendloop +
                ", sendstatus='" + sendstatus + '\'' +
                ", issend='" + issend + '\'' +
                ", createddate='" + createddate + '\'' +
                ", createduser='" + createduser + '\'' +
                ", CREATEDPAGE='" + CREATEDPAGE + '\'' +
                ", ipaddr='" + ipaddr + '\'' +
                ", kostl='" + kostl + '\'' +
                ", updateddate='" + updateddate + '\'' +
                ", updatedpage='" + updatedpage + '\'' +
                ", updateduser='" + updateduser + '\'' +
                ", recordstatus='" + recordstatus + '\'' +
                ", isactive='" + isactive + '\'' +
                '}';
    }
}
