package com.newgfmisinterface.rpinterfaceebidding.dto;

public class EbiddingHeaderItemDto {
    private String headerFile;
    private String typeFile;
    private String dateFile;

    public String getHeaderFile() {
        return headerFile;
    }

    public void setHeaderFile(String headerFile) {
        this.headerFile = headerFile;
    }

    public String getTypeFile() {
        return typeFile;
    }

    public void setTypeFile(String typeFile) {
        this.typeFile = typeFile;
    }

    public String getDateFile() {
        return dateFile;
    }

    public void setDateFile(String dateFile) {
        this.dateFile = dateFile;
    }

    @Override
    public String toString() {
        return "EbiddingHeaderItemDto{" +
                "headerFile='" + headerFile + '\'' +
                ", typeFile='" + typeFile + '\'' +
                ", dateFile='" + dateFile + '\'' +
                '}';
    }
}
