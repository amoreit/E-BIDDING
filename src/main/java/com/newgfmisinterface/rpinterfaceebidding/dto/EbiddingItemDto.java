package com.newgfmisinterface.rpinterfaceebidding.dto;

public class EbiddingItemDto {
    private String filename;
    private String transdate;
    private String doctype;
    private Integer itemno;
    private String cct;
    private String cctowner;
    private String xref3;
    private String zzdeposit;
    private String zzowner;
    private String amount;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getTransdate() {
        return transdate;
    }

    public void setTransdate(String transdate) {
        this.transdate = transdate;
    }

    public String getDoctype() {
        return doctype;
    }

    public void setDoctype(String doctype) {
        this.doctype = doctype;
    }

    public Integer getItemno() {
        return itemno;
    }

    public void setItemno(Integer itemno) {
        this.itemno = itemno;
    }

    public String getCct() {
        return cct;
    }

    public void setCct(String cct) {
        this.cct = cct;
    }

    public String getCctowner() {
        return cctowner;
    }

    public void setCctowner(String cctowner) {
        this.cctowner = cctowner;
    }

    public String getXref3() {
        return xref3;
    }

    public void setXref3(String xref3) {
        this.xref3 = xref3;
    }

    public String getZzdeposit() {
        return zzdeposit;
    }

    public void setZzdeposit(String zzdeposit) {
        this.zzdeposit = zzdeposit;
    }

    public String getZzowner() {
        return zzowner;
    }

    public void setZzowner(String zzowner) {
        this.zzowner = zzowner;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "EbiddingItemDto{" +
                "filename='" + filename + '\'' +
                ", transdate='" + transdate + '\'' +
                ", doctype='" + doctype + '\'' +
                ", itemno=" + itemno +
                ", cct='" + cct + '\'' +
                ", cctowner='" + cctowner + '\'' +
                ", xref3='" + xref3 + '\'' +
                ", zzdeposit='" + zzdeposit + '\'' +
                ", zzowner='" + zzowner + '\'' +
                ", amount='" + amount + '\'' +
                '}';
    }
}
