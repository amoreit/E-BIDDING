package com.newgfmisinterface.rpinterfaceebidding.dto;

public class EbiddingStatementDto {
    private String transdate;
    private String totalamt;
    private String amount01;
    private String amount02;
    private String amount03;
    private String amount04;
    private String amount05;

    public String getTransdate() {
        return transdate;
    }

    public void setTransdate(String transdate) {
        this.transdate = transdate;
    }

    public String getTotalamt() {
        return totalamt;
    }

    public void setTotalamt(String totalamt) {
        this.totalamt = totalamt;
    }

    public String getAmount01() {
        return amount01;
    }

    public void setAmount01(String amount01) {
        this.amount01 = amount01;
    }

    public String getAmount02() {
        return amount02;
    }

    public void setAmount02(String amount02) {
        this.amount02 = amount02;
    }

    public String getAmount03() {
        return amount03;
    }

    public void setAmount03(String amount03) {
        this.amount03 = amount03;
    }

    public String getAmount04() {
        return amount04;
    }

    public void setAmount04(String amount04) {
        this.amount04 = amount04;
    }

    public String getAmount05() {
        return amount05;
    }

    public void setAmount05(String amount05) {
        this.amount05 = amount05;
    }

    @Override
    public String toString() {
        return "EbiddingStatementDto{" +
                "transdate='" + transdate + '\'' +
                ", totalamt='" + totalamt + '\'' +
                ", amount01='" + amount01 + '\'' +
                ", amount02='" + amount02 + '\'' +
                ", amount03='" + amount03 + '\'' +
                ", amount04='" + amount04 + '\'' +
                ", amount05='" + amount05 + '\'' +
                '}';
    }
}
