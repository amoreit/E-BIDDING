package com.newgfmisinterface.rpinterfaceebidding.dto;

public class EbiddingFooterItemDto {
    private String footerFile;
    private String typeFile;
    private String totalRecord;
    private String totalAmount;
    private String dateFile;

    public String getFooterFile() {
        return footerFile;
    }

    public void setFooterFile(String footerFile) {
        this.footerFile = footerFile;
    }

    public String getTypeFile() {
        return typeFile;
    }

    public void setTypeFile(String typeFile) {
        this.typeFile = typeFile;
    }

    public String getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(String totalRecord) {
        this.totalRecord = totalRecord;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getDateFile() {
        return dateFile;
    }

    public void setDateFile(String dateFile) {
        this.dateFile = dateFile;
    }

    @Override
    public String toString() {
        return "EbiddingFooterItemDto{" +
                "footerFile='" + footerFile + '\'' +
                ", typeFile='" + typeFile + '\'' +
                ", totalRecord='" + totalRecord + '\'' +
                ", totalAmount='" + totalAmount + '\'' +
                ", dateFile='" + dateFile + '\'' +
                '}';
    }
}
