package com.newgfmisinterface.rpinterfaceebidding.dto;

import javax.xml.bind.annotation.XmlElement;

public class MQResponseErrorsDto {
    private MQResponseErrorLineDto error;

    public MQResponseErrorsDto() {
        super();
    }
    public MQResponseErrorsDto(MQResponseErrorLineDto error) {
        super();
        this.error = error;
    }
    public MQResponseErrorLineDto getError() {
        return error;
    }
    @XmlElement(name = "ERROR")
    public void setError(MQResponseErrorLineDto error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "MQResponseErrorsDto{" +
                "error=" + error +
                '}';
    }
}
