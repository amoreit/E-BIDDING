package com.newgfmisinterface.rpinterfaceebidding.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "EBIDDING_SENDDOCNO")
public class MqMsgReplyDto {
    private String id;

    private String type;

    private String to;

    private XmlDataReply data;

    public MqMsgReplyDto() {
        super();
    }


    public String getId() {
        return id;
    }


    @XmlElement(name = "ID")
    public void setId(String id) {
        this.id = id;
    }


    public String getType() {
        return type;
    }


    @XmlElement(name = "TYPE")
    public void setType(String type) {
        this.type = type;
    }


    public String getTo() {
        return to;
    }


    @XmlElement(name = "TO")
    public void setTo(String to) {
        this.to = to;
    }


    public XmlDataReply getData() {
        return data;
    }


    @XmlElement(name = "DATA")
    public void setData(XmlDataReply data) {
        this.data = data;
    }


    @Override
    public String toString() {
        return "MqMsgReplyDto [id=" + id + ", type=" + type + ", to=" + to + ", data=" + data + "]";
    }

}
