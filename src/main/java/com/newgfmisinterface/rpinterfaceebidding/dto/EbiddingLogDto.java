package com.newgfmisinterface.rpinterfaceebidding.dto;

public class EbiddingLogDto {
    private String filename;
    private String transdate;
    private String doctype;
    private String status;
    private Number totamt;
    private Number totrec;
    private Number postrec;
    private Number errrec;
    private String uploadname;
    private String uploadstart;
    private String uploadfinish;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getTransdate() {
        return transdate;
    }

    public void setTransdate(String transdate) {
        this.transdate = transdate;
    }

    public String getDoctype() {
        return doctype;
    }

    public void setDoctype(String doctype) {
        this.doctype = doctype;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Number getTotamt() {
        return totamt;
    }

    public void setTotamt(Number totamt) {
        this.totamt = totamt;
    }

    public Number getTotrec() {
        return totrec;
    }

    public void setTotrec(Number totrec) {
        this.totrec = totrec;
    }

    public Number getPostrec() {
        return postrec;
    }

    public void setPostrec(Number postrec) {
        this.postrec = postrec;
    }

    public Number getErrrec() {
        return errrec;
    }

    public void setErrrec(Number errrec) {
        this.errrec = errrec;
    }

    public String getUploadname() {
        return uploadname;
    }

    public void setUploadname(String uploadname) {
        this.uploadname = uploadname;
    }

    public String getUploadstart() {
        return uploadstart;
    }

    public void setUploadstart(String uploadstart) {
        this.uploadstart = uploadstart;
    }

    public String getUploadfinish() {
        return uploadfinish;
    }

    public void setUploadfinish(String uploadfinish) {
        this.uploadfinish = uploadfinish;
    }

    @Override
    public String toString() {
        return "EbiddingLogDto{" +
                "filename='" + filename + '\'' +
                ", transdate='" + transdate + '\'' +
                ", doctype='" + doctype + '\'' +
                ", status='" + status + '\'' +
                ", totamt=" + totamt +
                ", totrec=" + totrec +
                ", postrec=" + postrec +
                ", errrec=" + errrec +
                ", uploadname='" + uploadname + '\'' +
                ", uploadstart='" + uploadstart + '\'' +
                ", uploadfinish='" + uploadfinish + '\'' +
                '}';
    }
}
