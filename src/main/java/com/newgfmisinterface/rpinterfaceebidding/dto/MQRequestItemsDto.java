package com.newgfmisinterface.rpinterfaceebidding.dto;

import javax.xml.bind.annotation.XmlElement;

public class MQRequestItemsDto {
    private MQRequestItemDto item;

    public MQRequestItemsDto() {
        super();
    }
    public MQRequestItemsDto(MQRequestItemDto item) {
        super();
        this.item = item;
    }
    public MQRequestItemDto getItem() {
        return item;
    }
    @XmlElement(name = "ITEM")
    public void setItem(MQRequestItemDto item) {
        this.item = item;
    }

    @Override
    public String toString() {
        return "MQRequestItemsDto{" +
                "item=" + item +
                '}';
    }
}
