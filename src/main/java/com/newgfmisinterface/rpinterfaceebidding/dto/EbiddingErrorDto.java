package com.newgfmisinterface.rpinterfaceebidding.dto;

public class EbiddingErrorDto {
    private String filename;
    private String transdate;
    private String doctype;
    private Integer itemno;
    private String msgno;
    private String msgid;
    private String message;
    private String uploadname;
    private String uploaddate;
    private Integer errorno;
    private String isactive;

    public Integer getErrorno() {return errorno;}

    public void setErrorno(Integer errorno) {this.errorno = errorno;}

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getTransdate() {
        return transdate;
    }

    public void setTransdate(String transdate) {
        this.transdate = transdate;
    }

    public String getDoctype() {
        return doctype;
    }

    public void setDoctype(String doctype) {
        this.doctype = doctype;
    }

    public Integer getItemno() {
        return itemno;
    }

    public void setItemno(Integer itemno) {
        this.itemno = itemno;
    }

    public String getMsgno() {
        return msgno;
    }

    public void setMsgno(String msgno) {
        this.msgno = msgno;
    }

    public String getMsgid() {
        return msgid;
    }

    public void setMsgid(String msgid) {
        this.msgid = msgid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUploadname() {
        return uploadname;
    }

    public void setUploadname(String uploadname) {
        this.uploadname = uploadname;
    }

    public String getUploaddate() {
        return uploaddate;
    }

    public void setUploaddate(String uploaddate) {
        this.uploaddate = uploaddate;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    @Override
    public String toString() {
        return "EbiddingErrorDto{" +
                "filename='" + filename + '\'' +
                ", transdate='" + transdate + '\'' +
                ", doctype='" + doctype + '\'' +
                ", itemno=" + itemno +
                ", msgno='" + msgno + '\'' +
                ", msgid='" + msgid + '\'' +
                ", message='" + message + '\'' +
                ", uploadname='" + uploadname + '\'' +
                ", uploaddate='" + uploaddate + '\'' +
                ", errorno=" + errorno +
                ", isactive='" + isactive + '\'' +
                '}';
    }
}
