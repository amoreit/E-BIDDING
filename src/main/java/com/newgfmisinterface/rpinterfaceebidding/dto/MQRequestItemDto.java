package com.newgfmisinterface.rpinterfaceebidding.dto;

import javax.xml.bind.annotation.XmlElement;
public class MQRequestItemDto {
    private String postingKey = "";
    private String accType = "";
    private String DrCr = "";
    private String glAcc = "";
    private String fiArea = "";
    private String costCenter = "";
    private String fundSource = "";
    private String bgCode = "";
    private String bgActivity = "";
    private String costActivity = "";
    private String amount = "";
    private String reference3 = "";
    private String assignment = "";
    private String brDocNo = "";
    private String brLine;
    private String bankBook = "";
    private String gpsc = "";
    private String subAcc = "";
    private String subAccOwner = "";
    private String paymentCenter = "";
    private String depositAccOwner = "";
    private String depositAcc = "";
    private String lineItemText = "";
    private String lineDesc = "";
    private String paymentTerm = "";
    private String paymentMethod = "";
    private String wtxType = "";
    private String wtxCode = "";
    private String wtxBase = "";
    private String wtxAmount = "";
    private String wtxTypeP = "";
    private String wtxCodeP = "";
    private String wtxBaseP = "";
    private String wtxAmountP = "";
    private String vendor = "";
    private String vendorTaxid = "";
    private String bankAccNo = "";
    private String bankBranchNo = "";
    private String tradingPartner = "";
    private String tradingPartnerPark = "";
    private String gpscGroup = "";
    private String specialGl = "";
    private String creditMemoDocNo = "";
    private String creditMemoFiscalYear = "";
    private String assetNo = "";
    private String assetSubNo = "";
    private String uom = "";
    private String uomIso = "";
    private String reference1 = "";
    private String reference2 = "";
    private String compCode = "";
    private String poDocNo = "";
    private String poLine = "";
    private String income = "";
    private String paymentBlock = "";
    private String paymentRef = "";
    private String autogen = "";
    private String isWtx = "";
    private String line = "";
    private String fundCenter = "";
    private String dateBaseline = "";
    private String dateValue = "";


    public MQRequestItemDto() {
        super();
    }
    public MQRequestItemDto(String postingKey, String accType, String DrCr, String glAcc, String fiArea,
                            String costCenter, String fundSource, String bgCode, String bgActivity, String costActivity,
                            String amount, String reference3, String assignment, String brDocNo, String brLine,
                            String bankBook, String gpsc, String subAcc, String subAccOwner, String paymentCenter,
                            String depositAccOwner, String depositAcc, String lineItemText, String lineDesc, String paymentTerm,
                            String paymentMethod, String wtxType, String wtxCode, String wtxBase, String wtxAmount,
                            String wtxTypeP, String wtxCodeP, String wtxBaseP, String wtxAmountP, String vendor,
                            String vendorTaxid, String bankAccNo, String bankBranchNo, String tradingPartner, String tradingPartnerPark,
                            String gpscGroup, String specialGl, String creditMemoDocNo, String creditMemoFiscalYear, String assetNo,
                            String assetSubNo, String uom, String uomIso, String reference1, String reference2,
                            String COMP_CODE, String poDocNo, String poLine, String income, String paymentBlock,
                            String paymentRef, String autogen, String isWtx, String line,String fundCenter,String dateBaseline,String dateValue) {
        super();
        this.postingKey = postingKey;
        this.accType = accType;
        this.DrCr = DrCr;
        this.glAcc = glAcc;
        this.fiArea = fiArea;
        this.costCenter = costCenter;
        this.fundSource = fundSource;
        this.bgCode = bgCode;
        this.bgActivity = bgActivity;
        this.costActivity = costActivity;
        this.amount = amount;
        this.reference3 = reference3;
        this.assignment = assignment;
        this.brDocNo = brDocNo;
        this.brLine = brLine;
        this.bankBook = bankBook;
        this.gpsc = gpsc;
        this.subAcc = subAcc;
        this.subAccOwner = subAccOwner;
        this.paymentCenter = paymentCenter;
        this.depositAccOwner = depositAccOwner;
        this.depositAcc = depositAcc;
        this.lineItemText = lineItemText;
        this.lineDesc = lineDesc;
        this.paymentTerm = paymentTerm;
        this.paymentMethod = paymentMethod;
        this.wtxType = wtxType;
        this.wtxCode = wtxCode;
        this.wtxBase = wtxBase;
        this.wtxAmount = wtxAmount;
        this.wtxTypeP = wtxTypeP;
        this.wtxCodeP = wtxCodeP;
        this.wtxBaseP = wtxBaseP;
        this.wtxAmountP = wtxAmountP;
        this.vendor = vendor;
        this.vendorTaxid = vendorTaxid;
        this.bankAccNo = bankAccNo;
        this.bankBranchNo = bankBranchNo;
        this.tradingPartner = tradingPartner;
        this.tradingPartnerPark = tradingPartnerPark;
        this.gpscGroup = gpscGroup;
        this.specialGl = specialGl;
        this.creditMemoDocNo = creditMemoDocNo;
        this.creditMemoFiscalYear = creditMemoFiscalYear;
        this.assetNo = assetNo;
        this.assetSubNo = assetSubNo;
        this.uom = uom;
        this.uomIso = uomIso;
        this.reference1 = reference1;
        this.reference2 = reference2;
        this.compCode = COMP_CODE;
        this.poDocNo = poDocNo;
        this.poLine = poLine;
        this.income = income;
        this.paymentBlock = paymentBlock;
        this.paymentRef = paymentRef;
        this.autogen = autogen;
        this.isWtx = isWtx;
        this.line = line;
        this.fundCenter = fundCenter;
        this.dateBaseline = dateBaseline;
        this.dateValue = dateValue;
    }


    public String getDrCr() {
        return DrCr;
    }
    @XmlElement(name = "ITEM")
    public void setDrCr(String drCr) {
        DrCr = drCr;
    }

    public String getReference3() {
        return reference3;
    }
    @XmlElement(name = "REFERENCE3")
    public void setReference3(String reference3) {
        this.reference3 = reference3;
    }

    public String getBrDocNo() {
        return brDocNo;
    }
    @XmlElement(name = "BR_DOC_NO")
    public void setBrDocNo(String brDocNo) {
        this.brDocNo = brDocNo;
    }

    public String getBrLine() {
        return brLine;
    }
    @XmlElement(name = "BR_LINE")
    public void setBrLine(String brLine) {
        this.brLine = brLine;
    }

    public String getGpsc() {
        return gpsc;
    }
    @XmlElement(name = "GPSC")
    public void setGpsc(String gpsc) {
        this.gpsc = gpsc;
    }

    public String getWtxTypeP() {
        return wtxTypeP;
    }
    @XmlElement(name = "WTX_TYPE_P")
    public void setWtxTypeP(String wtxTypeP) {
        this.wtxTypeP = wtxTypeP;
    }

    public String getWtxCodeP() {
        return wtxCodeP;
    }
    @XmlElement(name = "WTX_CODE_P")
    public void setWtxCodeP(String wtxCodeP) {
        this.wtxCodeP = wtxCodeP;
    }

    public String getWtxBaseP() {
        return wtxBaseP;
    }
    @XmlElement(name = "WTX_BASE_P")
    public void setWtxBaseP(String wtxBaseP) {
        this.wtxBaseP = wtxBaseP;
    }

    public String getWtxAmountP() {
        return wtxAmountP;
    }
    @XmlElement(name = "WTX_AMOUNT_P")
    public void setWtxAmountP(String wtxAmountP) {
        this.wtxAmountP = wtxAmountP;
    }

    public String getVendor() {
        return vendor;
    }
    @XmlElement(name = "VENDOR")
    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getTradingPartnerPark() {
        return tradingPartnerPark;
    }
    @XmlElement(name = "TRADING_PARTNER_PARK")
    public void setTradingPartnerPark(String tradingPartnerPark) {
        this.tradingPartnerPark = tradingPartnerPark;
    }

    public String getGpscGroup() {
        return gpscGroup;
    }
    @XmlElement(name = "GPSC_GROUP")
    public void setGpscGroup(String gpscGroup) {
        this.gpscGroup = gpscGroup;
    }

    public String getCreditMemoDocNo() {
        return creditMemoDocNo;
    }
    @XmlElement(name = "CREDIT_MEMO_DOC_NO")
    public void setCreditMemoDocNo(String creditMemoDocNo) {
        this.creditMemoDocNo = creditMemoDocNo;
    }

    public String getCreditMemoFiscalYear() {
        return creditMemoFiscalYear;
    }
    @XmlElement(name = "CREDIT_MEMO_FISCAL_YEAR")
    public void setCreditMemoFiscalYear(String creditMemoFiscalYear) {
        this.creditMemoFiscalYear = creditMemoFiscalYear;
    }

    public String getAssetNo() {
        return assetNo;
    }
    @XmlElement(name = "ASSET_NO")
    public void setAssetNo(String assetNo) {
        this.assetNo = assetNo;
    }

    public String getAssetSubNo() {
        return assetSubNo;
    }
    @XmlElement(name = "ASSET_SUB_NO")
    public void setAssetSubNo(String assetSubNo) {
        this.assetSubNo = assetSubNo;
    }

    public String getUom() {
        return uom;
    }
    @XmlElement(name = "UOM")
    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getUomIso() {
        return uomIso;
    }
    @XmlElement(name = "UOM_ISO")
    public void setUomIso(String uomIso) {
        this.uomIso = uomIso;
    }

    public String getReference2() {
        return reference2;
    }
    @XmlElement(name = "REFERENCE2")
    public void setReference2(String reference2) {
        this.reference2 = reference2;
    }

    public String getCompCode() {
        return compCode;
    }
    @XmlElement(name = "COMP_CODE")
    public void setCompCode(String compCode) {
        this.compCode = compCode;
    }

    public String getPoDocNo() {
        return poDocNo;
    }
    @XmlElement(name = "PO_DOC_NO")
    public void setPoDocNo(String poDocNo) {
        this.poDocNo = poDocNo;
    }

    public String getPoLine() {
        return poLine;
    }
    @XmlElement(name = "PO_LINE")
    public void setPoLine(String poLine) {
        this.poLine = poLine;
    }

    public String getPaymentBlock() {
        return paymentBlock;
    }
    @XmlElement(name = "PAYMENT_BLOCK")
    public void setPaymentBlock(String paymentBlock) {
        this.paymentBlock = paymentBlock;
    }

    public String getPaymentRef() {
        return paymentRef;
    }
    @XmlElement(name = "PAYMENT_REF")
    public void setPaymentRef(String paymentRef) {
        this.paymentRef = paymentRef;
    }

    public String getAutogen() {
        return autogen;
    }
    @XmlElement(name = "AUTOGEN")
    public void setAutogen(String autogen) {
        this.autogen = autogen;
    }

    public String getIsWtx() {
        return isWtx;
    }
    @XmlElement(name = "IS_WTX")
    public void setIsWtx(String isWtx) {
        this.isWtx = isWtx;
    }

    public String getLine() {
        return line;
    }
    @XmlElement(name = "LINE")
    public void setLine(String line) {
        this.line = line;
    }

    public String getPostingKey() {
        return postingKey;
    }
    @XmlElement(name = "POSTING_KEY")
    public void setPostingKey(String postingKey) {
        this.postingKey = postingKey;
    }

    public String getAccType() {
        return accType;
    }
    @XmlElement(name = "ACC_TYPE")
    public void setAccType(String accType) {
        this.accType = accType;
    }

    public String getGlAcc() {
        return glAcc;
    }
    @XmlElement(name = "GL_ACC")
    public void setGlAcc(String glAcc) {
        this.glAcc = glAcc;
    }

    public String getFiArea() {
        return fiArea;
    }
    @XmlElement(name = "FI_AREA")
    public void setFiArea(String fiArea) {
        this.fiArea = fiArea;
    }

    public String getCostCenter() {
        return costCenter;
    }
    @XmlElement(name = "COST_CENTER")
    public void setCostCenter(String costCenter) {
        this.costCenter = costCenter;
    }

    public String getFundSource() {
        return fundSource;
    }
    @XmlElement(name = "FUND_SOURCE")
    public void setFundSource(String fundSource) {
        this.fundSource = fundSource;
    }

    public String getFundCenter() {
        return fundCenter;
    }
    @XmlElement(name = "FUND_CENTER")
    public void setFundCenter(String fundCenter) {
        this.fundCenter = fundCenter;
    }

    public String getBgCode() {
        return bgCode;
    }
    @XmlElement(name = "BG_CODE")
    public void setBgCode(String bgCode) {
        this.bgCode = bgCode;
    }

    public String getBgActivity() {
        return bgActivity;
    }
    @XmlElement(name = "BG_ACTIVITY")
    public void setBgActivity(String bgActivity) {
        this.bgActivity = bgActivity;
    }

    public String getCostActivity() {
        return costActivity;
    }
    @XmlElement(name = "COST_ACTIVITY")
    public void setCostActivity(String costActivity) {
        this.costActivity = costActivity;
    }

    public String getAmount() {
        return amount;
    }
    @XmlElement(name = "AMOUNT")
    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAssignment() {
        return assignment;
    }
    @XmlElement(name = "ASSIGNMENT")
    public void setAssignment(String assignment) {
        this.assignment = assignment;
    }

    public String getBankBook() {
        return bankBook;
    }
    @XmlElement(name = "BANK_BOOK")
    public void setBankBook(String bankBook) {
        this.bankBook = bankBook;
    }

    public String getSubAcc() {
        return subAcc;
    }
    @XmlElement(name = "SUB_ACC")
    public void setSubAcc(String subAcc) {
        this.subAcc = subAcc;
    }

    public String getSubAccOwner() {
        return subAccOwner;
    }
    @XmlElement(name = "SUB_ACC_OWNER")
    public void setSubAccOwner(String subAccOwner) {
        this.subAccOwner = subAccOwner;
    }

    public String getPaymentCenter() {
        return paymentCenter;
    }
    @XmlElement(name = "PAYMENT_CENTER")
    public void setPaymentCenter(String paymentCenter) {
        this.paymentCenter = paymentCenter;
    }

    public String getDepositAccOwner() {
        return depositAccOwner;
    }
    @XmlElement(name = "DEPOSIT_ACC_OWNER")
    public void setDepositAccOwner(String depositAccOwner) {
        this.depositAccOwner = depositAccOwner;
    }

    public String getDepositAcc() {
        return depositAcc;
    }
    @XmlElement(name = "DEPOSIT_ACC")
    public void setDepositAcc(String depositAcc) {
        this.depositAcc = depositAcc;
    }

    public String getLineItemText() {
        return lineItemText;
    }
    @XmlElement(name = "LINE_ITEM_TEXT")
    public void setLineItemText(String lineItemText) {
        this.lineItemText = lineItemText;
    }

    public String getLineDesc() {
        return lineDesc;
    }
    @XmlElement(name = "LINE_DESC")
    public void setLineDesc(String lineDesc) {
        this.lineDesc = lineDesc;
    }

    public String getPaymentTerm() {
        return paymentTerm;
    }
    @XmlElement(name = "PAYMENT_TERM")
    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }
    @XmlElement(name = "PAYMENT_METHOD")
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getWtxType() {
        return wtxType;
    }
    @XmlElement(name = "WTX_TYPE")
    public void setWtxType(String wtxType) {
        this.wtxType = wtxType;
    }

    public String getWtxCode() {
        return wtxCode;
    }
    @XmlElement(name = "WTX_CODE")
    public void setWtxCode(String wtxCode) {
        this.wtxCode = wtxCode;
    }

    public String getWtxBase() {
        return wtxBase;
    }
    @XmlElement(name = "WTX_BASE")
    public void setWtxBase(String wtxBase) {
        this.wtxBase = wtxBase;
    }

    public String getWtxAmount() {
        return wtxAmount;
    }
    @XmlElement(name = "WTX_AMOUNT")
    public void setWtxAmount(String wtxAmount) {
        this.wtxAmount = wtxAmount;
    }

    public String getVendorTaxid() {
        return vendorTaxid;
    }
    @XmlElement(name = "VENDOR_TAXID")
    public void setVendorTaxid(String vendorTaxid) {
        this.vendorTaxid = vendorTaxid;
    }

    public String getBankAccNo() {
        return bankAccNo;
    }
    @XmlElement(name = "BANK_ACC_NO")
    public void setBankAccNo(String bankAccNo) {
        this.bankAccNo = bankAccNo;
    }

    public String getBankBranchNo() {
        return bankBranchNo;
    }
    @XmlElement(name = "BANK_BRANCH_NO")
    public void setBankBranchNo(String bankBranchNo) {
        this.bankBranchNo = bankBranchNo;
    }

    public String getTradingPartner() {
        return tradingPartner;
    }
    @XmlElement(name = "TRADING_PARTNER")
    public void setTradingPartner(String tradingPartner) {
        this.tradingPartner = tradingPartner;
    }

    public String getSpecialGl() {
        return specialGl;
    }
    @XmlElement(name = "SPECIAL_GL")
    public void setSpecialGl(String specialGl) {
        this.specialGl = specialGl;
    }

    public String getDateBaseline() {
        return dateBaseline;
    }
    @XmlElement(name = "DATE_BASELINE")
    public void setDateBaseline(String dateBaseline) {
        this.dateBaseline = dateBaseline;
    }

    public String getDateValue() {
        return dateValue;
    }
    @XmlElement(name = "DATE_VALUE")
    public void setDateValue(String dateValue) {
        this.dateValue = dateValue;
    }

    public String getReference1() {
        return reference1;
    }
    @XmlElement(name = "REFERENCE1")
    public void setReference1(String reference1) {
        this.reference1 = reference1;
    }

    public String getIncome() {
        return income;
    }
    @XmlElement(name = "INCOME")
    public void setIncome(String income) {
        this.income = income;
    }

    @Override
    public String toString() {
        return "MQRequestItemDto{" +
                "postingKey='" + postingKey + '\'' +
                ", accType='" + accType + '\'' +
                ", DrCr='" + DrCr + '\'' +
                ", glAcc='" + glAcc + '\'' +
                ", fiArea='" + fiArea + '\'' +
                ", costCenter='" + costCenter + '\'' +
                ", fundSource='" + fundSource + '\'' +
                ", bgCode='" + bgCode + '\'' +
                ", bgActivity='" + bgActivity + '\'' +
                ", costActivity='" + costActivity + '\'' +
                ", amount='" + amount + '\'' +
                ", reference3='" + reference3 + '\'' +
                ", assignment='" + assignment + '\'' +
                ", brDocNo='" + brDocNo + '\'' +
                ", brLine='" + brLine + '\'' +
                ", bankBook='" + bankBook + '\'' +
                ", gpsc='" + gpsc + '\'' +
                ", subAcc='" + subAcc + '\'' +
                ", subAccOwner='" + subAccOwner + '\'' +
                ", paymentCenter='" + paymentCenter + '\'' +
                ", depositAccOwner='" + depositAccOwner + '\'' +
                ", depositAcc='" + depositAcc + '\'' +
                ", lineItemText='" + lineItemText + '\'' +
                ", lineDesc='" + lineDesc + '\'' +
                ", paymentTerm='" + paymentTerm + '\'' +
                ", paymentMethod='" + paymentMethod + '\'' +
                ", wtxType='" + wtxType + '\'' +
                ", wtxCode='" + wtxCode + '\'' +
                ", wtxBase='" + wtxBase + '\'' +
                ", wtxAmount='" + wtxAmount + '\'' +
                ", wtxTypeP='" + wtxTypeP + '\'' +
                ", wtxCodeP='" + wtxCodeP + '\'' +
                ", wtxBaseP='" + wtxBaseP + '\'' +
                ", wtxAmountP='" + wtxAmountP + '\'' +
                ", vendor='" + vendor + '\'' +
                ", vendorTaxid='" + vendorTaxid + '\'' +
                ", bankAccNo='" + bankAccNo + '\'' +
                ", bankBranchNo='" + bankBranchNo + '\'' +
                ", tradingPartner='" + tradingPartner + '\'' +
                ", tradingPartnerPark='" + tradingPartnerPark + '\'' +
                ", gpscGroup='" + gpscGroup + '\'' +
                ", specialGl='" + specialGl + '\'' +
                ", creditMemoDocNo='" + creditMemoDocNo + '\'' +
                ", creditMemoFiscalYear='" + creditMemoFiscalYear + '\'' +
                ", assetNo='" + assetNo + '\'' +
                ", assetSubNo='" + assetSubNo + '\'' +
                ", uom='" + uom + '\'' +
                ", uomIso='" + uomIso + '\'' +
                ", reference1='" + reference1 + '\'' +
                ", reference2='" + reference2 + '\'' +
                ", compCode='" + compCode + '\'' +
                ", poDocNo='" + poDocNo + '\'' +
                ", poLine='" + poLine + '\'' +
                ", income='" + income + '\'' +
                ", paymentBlock='" + paymentBlock + '\'' +
                ", paymentRef='" + paymentRef + '\'' +
                ", autogen='" + autogen + '\'' +
                ", isWtx='" + isWtx + '\'' +
                ", line='" + line + '\'' +
                ", fundCenter='" + fundCenter + '\'' +
                ", dateBaseline='" + dateBaseline + '\'' +
                ", dateValue='" + dateValue + '\'' +
                '}';
    }
}
