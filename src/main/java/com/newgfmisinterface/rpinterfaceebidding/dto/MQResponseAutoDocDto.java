package com.newgfmisinterface.rpinterfaceebidding.dto;

import javax.xml.bind.annotation.XmlElement;

public class MQResponseAutoDocDto {
    private String errors = "";
    private String compCode = "";
    private String docType = "";
    private String accDocNo = "";
    private String fiscalYear = "";
    private String remark = "";

    public MQResponseAutoDocDto() {
        super();
    }
    public MQResponseAutoDocDto(String errors, String compCode, String docType, String accDocNo, String fiscalYear, String remark) {
        super();
        this.errors = errors;
        this.compCode = compCode;
        this.docType = docType;
        this.accDocNo = accDocNo;
        this.fiscalYear = fiscalYear;
        this.remark = remark;
    }
    public String getErrors() {
        return errors;
    }
    @XmlElement(name = "ERRORS")
    public void setErrors(String errors) {
        this.errors = errors;
    }

    public String getCompCode() {
        return compCode;
    }
    @XmlElement(name = "COMP_CODE")
    public void setCompCode(String compCode) {
        this.compCode = compCode;
    }

    public String getDocType() {
        return docType;
    }
    @XmlElement(name = "DOC_TYPE")
    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getAccDocNo() {
        return accDocNo;
    }
    @XmlElement(name = "ACC_DOC_NO")
    public void setAccDocNo(String accDocNo) {
        this.accDocNo = accDocNo;
    }

    public String getFiscalYear() {
        return fiscalYear;
    }
    @XmlElement(name = "FISCAL_YEAR")
    public void setFiscalYear(String fiscalYear) {
        this.fiscalYear = fiscalYear;
    }

    public String getRemark() {
        return remark;
    }
    @XmlElement(name = "REMARK")
    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "MQResponseAutoDocDto{" +
                "errors='" + errors + '\'' +
                ", compCode='" + compCode + '\'' +
                ", docType='" + docType + '\'' +
                ", accDocNo='" + accDocNo + '\'' +
                ", fiscalYear='" + fiscalYear + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
