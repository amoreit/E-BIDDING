package com.newgfmisinterface.rpinterfaceebidding.dto;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "RESPONSE")
public class MQResponseDocDto {
    private MQResponseErrorsDto errors = null;
    private String compCode = "";
    private String accDocNo = "";
    private String fiscalYear = "";
    private String docType = "";
    private String docStatus = "";
    private String docStatusName = "";
    private String revAccDocNo = "";
    private String revFiscalYear = "";
    private String revDocType = "";
    private String remark = "";
    private MQResponseAutoDocsDto autodocs = null;

    public MQResponseErrorsDto getErrors() {
        return errors;
    }
    @XmlElement(name = "ERRORS")
    public void setErrors(MQResponseErrorsDto errors) {
        this.errors = errors;
    }

    public String getCompCode() {
        return compCode;
    }
    @XmlElement(name = "COMP_CODE")
    public void setCompCode(String compCode) {
        this.compCode = compCode;
    }

    public String getAccDocNo() {
        return accDocNo;
    }
    @XmlElement(name = "ACC_DOC_NO")
    public void setAccDocNo(String accDocNo) {
        this.accDocNo = accDocNo;
    }

    public String getFiscalYear() {
        return fiscalYear;
    }
    @XmlElement(name = "FISCAL_YEAR")
    public void setFiscalYear(String fiscalYear) {
        this.fiscalYear = fiscalYear;
    }

    public String getDocType() {
        return docType;
    }
    @XmlElement(name = "DOC_TYPE")
    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getDocStatus() {
        return docStatus;
    }
    @XmlElement(name = "DOC_STATUS")
    public void setDocStatus(String docStatus) {
        this.docStatus = docStatus;
    }

    public String getDocStatusName() {
        return docStatusName;
    }
    @XmlElement(name = "DOC_STATUS_NAME")
    public void setDocStatusName(String docStatusName) {
        this.docStatusName = docStatusName;
    }

    public String getRevAccDocNo() {
        return revAccDocNo;
    }
    @XmlElement(name = "REV_ACC_DOC_NO")
    public void setRevAccDocNo(String revAccDocNo) {
        this.revAccDocNo = revAccDocNo;
    }

    public String getRevFiscalYear() {
        return revFiscalYear;
    }
    @XmlElement(name = "REV_FISCAL_YEAR")
    public void setRevFiscalYear(String revFiscalYear) {
        this.revFiscalYear = revFiscalYear;
    }

    public String getRevDocType() {
        return revDocType;
    }
    @XmlElement(name = "REV_DOC_TYPE")
    public void setRevDocType(String revDocType) {
        this.revDocType = revDocType;
    }

    public String getRemark() {
        return remark;
    }
    @XmlElement(name = "REMARK")
    public void setRemark(String remark) {
        this.remark = remark;
    }

    public MQResponseAutoDocsDto getAutodocs() {
        return autodocs;
    }
    @XmlElement(name = "AUTODOCS")
    public void setAutodocs(MQResponseAutoDocsDto autodocs) {
        this.autodocs = autodocs;
    }

    @Override
    public String toString() {
        return "MQResponseDocDto [" +
                "errors=" + errors +
                ", compCode='" + compCode + '\'' +
                ", accDocNo='" + accDocNo + '\'' +
                ", fiscalYear='" + fiscalYear + '\'' +
                ", docType='" + docType + '\'' +
                ", docStatus='" + docStatus + '\'' +
                ", docStatusName='" + docStatusName + '\'' +
                ", revAccDocNo='" + revAccDocNo + '\'' +
                ", revFiscalYear='" + revFiscalYear + '\'' +
                ", revDocType='" + revDocType + '\'' +
                ", remark='" + remark + '\'' +
                ", autodocs=" + autodocs +
                ']';
    }
}
