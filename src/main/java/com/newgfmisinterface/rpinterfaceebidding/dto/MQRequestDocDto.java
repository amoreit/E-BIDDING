package com.newgfmisinterface.rpinterfaceebidding.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "REQUEST")
public class MQRequestDocDto {
    private String parkPost = "";
    private String flag = "";
    private String formId = "";
    private String autoDoc = "";
    private MQRequestHeaderDto header;
    private MQRequestItemsDto items;
    private MQRequestFromDocDto firstHeader;
    private String exitsCompletes = "";

    public String getParkPost() {
        return parkPost;
    }

    @XmlElement(name = "PARK_POST")
    public void setParkPost(String parkPost) {
        this.parkPost = parkPost;
    }

    public String getFlag() {
        return flag;
    }

    @XmlElement(name = "FLAG")
    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getFormId() {
        return formId;
    }

    @XmlElement(name = "FORMID")
    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getAutoDoc() {
        return autoDoc;
    }

    @XmlElement(name = "AUTODOC")
    public void setAutoDoc(String autoDoc) {
        this.autoDoc = autoDoc;
    }

    public MQRequestHeaderDto getHeader() {
        return header;
    }

    @XmlElement(name = "HEADER")
    public void setHeader(MQRequestHeaderDto header) {
        this.header = header;
    }

    public MQRequestItemsDto getItems() {
        return items;
    }

    @XmlElement(name = "ITEMS")
    public void setItems(MQRequestItemsDto items) {
        this.items = items;
    }

    public MQRequestFromDocDto getFirstHeader() {
        return firstHeader;
    }

    @XmlElement(name = "FIRSTHEADER")
    public void setFirstHeader(MQRequestFromDocDto firstHeader) {
        this.firstHeader = firstHeader;
    }

    public String getExitsCompletes() {
        return exitsCompletes;
    }

    @XmlElement(name = "EXITS_COMPLETES")
    public void setExitsCompletes(String exitsCompletes) {
        this.exitsCompletes = exitsCompletes;
    }

    @Override
    public String toString() {
        return "MQRequestDocDto [parkPost=" + parkPost + ", flag=" + flag + ", formId=" + formId + ", autoDoc=" + autoDoc + ", header=" + header + ", items=" + items + ", firstHeader=" + firstHeader + ", exitsCompletes=" + exitsCompletes + "]";
    }
}
