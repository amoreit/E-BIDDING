package com.newgfmisinterface.rpinterfaceebidding.controller;

import com.newgfmisinterface.rpinterfaceebidding.dao.*;
import com.newgfmisinterface.rpinterfaceebidding.dto.ResponseDto;
import com.newgfmisinterface.rpinterfaceebidding.job.ProcessJobEBD;
import com.newgfmisinterface.rpinterfaceebidding.job.ProcessJobSTM;
import com.newgfmisinterface.rpinterfaceebidding.job.ProcessJobAMQ;
import com.newgfmisinterface.rpinterfaceebidding.service.FileStorageService;
import com.newgfmisinterface.rpinterfaceebidding.service.ScanFilePathService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class ExecuteTaskController {
    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private ScanFilePathService scanFilePath;

    @Autowired
    private TaskScheduler taskScheduler;

    @Autowired
    private ProcessJobEBD processJobEBD;

    @Autowired
    private ProcessJobSTM processJobSTM;

    @Autowired
    private ProcessJobAMQ processJobAMQ;

    @Autowired
    private EbiddingLogDao ebiddingLogDao;

    @Autowired
    private EbiddingItemDao ebiddingItemDao;

    @Autowired
    private EbiddingStatementDao ebiddingStatementDao;

    @Autowired
    private EbiddingErrorDao ebiddingErrorDao;

    @Autowired
    private EbiddingActiveMQDao ebiddingActiveMQDao;

    @Autowired
    private EbiddingSendFileDao ebiddingSendFileDao;

    @GetMapping("/executetask")
    public ResponseEntity<String> executeTaskEBD() {
        long startTimeTotal = System.currentTimeMillis();

        Runnable taskProcessSTM = () -> processJobSTM.jobRunner();
        taskScheduler.schedule(taskProcessSTM, new Date());

        String[] projectFile = {"GFEBIDINGRCRD", "GFEBIDINGR3R4"};
        List<String> projectName = new ArrayList<>();
        int lengthProject = projectFile.length;
        for (String projectCode : projectFile) {

            long startTime = System.currentTimeMillis();
            List<String> moveFile = scanFilePath.listFilesForFolder(new File(fileStorageService.getFileInputLocation().toString()), projectCode);

            int fileListForScan = 0;
            for (String moveFileToProcess : moveFile) {
                File fileName = new File(moveFileToProcess);
                String getFileName = fileName.getName();
                projectName.add(getFileName);
                fileStorageService.moveFileToProcessDir(moveFileToProcess);
                fileListForScan++;
            }
            if (fileListForScan > 0) {
                List<String> fileList = scanFilePath.listFilesForFolder(new File(fileStorageService.getFileStorageLocation().toString()), projectCode);
                for (String filename : fileList) {
                    processJobEBD.jobRunner(filename);
                }
            }
            lengthProject--;

            long endTime = System.currentTimeMillis();
            long duration = (endTime - startTime);
            System.out.print("Execution time is " + duration + " milli seconds\n");

            if (lengthProject == 0) {
                for (String projectname : projectName) {
                    processJobAMQ.jobRunner(projectname, 1);
                    try {
                        int checkLoop = ebiddingActiveMQDao.countItemFileError(projectname);
                        if (checkLoop > 0) {
                            for (int i = 1; i <= 2; i++) {
                                processJobAMQ.jobRunner(projectname, 2);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        long endTimeTotal = System.currentTimeMillis();
        long durationTotal = (endTimeTotal - startTimeTotal);
        System.out.print("Completed time is " + durationTotal + " milli seconds");

        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        return new ResponseEntity<String>("<h1>Job Task e-bidding is Running at Time : " + timeStamp + "</h1>", HttpStatus.OK);
    }
//    @GetMapping("/convertFileEbidingPath")
//    public ResponseEntity<ResponseDto> getTrnCodeList(String provinceCode) {
//        List<?> resultList = null;
//        try {
//            System.out.println("<<< provinceCode: " + provinceCode);
//            resultList = ebiddingSendFileDao.getConvertFileEbidingPath(provinceCode);
//            System.out.println("<<< resultList: " + resultList);
//            return new ResponseEntity<ResponseDto>(new ResponseDto(1, "success", resultList), HttpStatus.OK);
//        } catch (Exception e) {
//            return new ResponseEntity<ResponseDto>(new ResponseDto(0, "fail", resultList), HttpStatus.NO_CONTENT);
//        }
//    }
    @GetMapping("/convertFileEbiddingPathInput")
    public ResponseDto getConvertFileEbiddingPathInput() {

        List<Map<String, Object>> resultList = null;
        try {
            resultList = ebiddingSendFileDao.getConvertFileEbiddingPathInput();
            System.out.println("<<< resultList: " + resultList);
            return new ResponseDto(1, "success", resultList);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseDto(0, "fail", resultList);
        }
    }
    @GetMapping("/convertFileEbiddingPathOutput")
    public ResponseDto getConvertFileEbiddingPathOutput() {

        List<Map<String, Object>> resultList = null;
        try {
            resultList = ebiddingSendFileDao.getConvertFileEbiddingPathOutput();
            System.out.println("<<< resultList: " + resultList);
            return new ResponseDto(1, "success", resultList);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseDto(0, "fail", resultList);
        }
    }
}
