package com.newgfmisinterface.rpinterfaceebidding;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.newgfmisinterface.rpinterfaceebidding.properties.FileStorageProperties;


@SpringBootApplication
@EnableConfigurationProperties({
    FileStorageProperties.class
})
public class RpinterfacejobscheduleApplication {

	public static void main(String[] args) {
		SpringApplication.run(RpinterfacejobscheduleApplication.class, args);
	}

}
