//package com.newgfmisinterface.rpinterfaceebidding.queue;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.jms.annotation.JmsListener;
//import org.springframework.messaging.handler.annotation.Header;
//import org.springframework.stereotype.Component;
//
//import com.newgfmisinterface.rpinterfaceebidding.dto.MqMsgDto;
//import com.newgfmisinterface.rpinterfaceebidding.utils.AES;
//
//import javax.xml.bind.JAXBContext;
//import javax.xml.bind.JAXBException;
//import javax.xml.bind.Unmarshaller;
//import javax.xml.transform.stream.StreamSource;
//import java.io.StringReader;
////import com.javasampleapproach.activemq.model.Product;
//
//@Component
//public class JmsConsumer {
//    private static final Logger LOGGER = LoggerFactory.getLogger(JmsConsumer.class);
////    @JmsListener(destination = "${spring.activemq.queue.consumer}", containerFactory = "jsaFactory")
//    @JmsListener(destination = "${spring.activemq.queue.consumer}")
//    public void appleReceive(String message) {
////    public void appleReceive(Product product, @Header("type") String productType){
////        if("iphone".equals(productType)){
////            System.out.println("Recieved Iphone: " + product);
////        }else if("ipad".equals(productType)){
////            System.out.println("Recieved Ipad: " + product);
////        }
//        final String secretKey = "123456789";
//
//    	String decryptedString = AES.decrypt(message, secretKey);
//
//    	LOGGER.info("received message not encrypte='{}'", decryptedString);
//
//    	MqMsgDto mqMsgObj = jaxbXmlMsgToObject(decryptedString);
//
//    	LOGGER.info("received message mqMsgObj ='{}'", mqMsgObj.toString());
//
//        try {
////            ebiddingActiveMQDao.updateLogMQCode(mqMsgObj);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    //    	public void receive(String message) {
////
////		LOGGER.info("received message='{}'", message);
////
////    	final String secretKey = "123456789";
////
////    	String decryptedString = AES.decrypt(message, secretKey);
////
////    	LOGGER.info("received message not encrypte='{}'", decryptedString);
////
////    	MqMsgDto mqMsgObj = jaxbXmlMsgToObject(decryptedString);
////
////    	LOGGER.info("received message mqMsgObj ='{}'", mqMsgObj.toString());
////
////        try {
////            ebiddingActiveMQDao.updateLogMQCode(mqMsgObj);
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
////
////        System.out.println("decryptedString"+decryptedString);
////	}
//    private static MqMsgDto jaxbXmlMsgToObject(String xmlmessage) {
//
//        JAXBContext jaxbContext;
//        MqMsgDto mqMsgDto;
//        try {
//            StreamSource streamSource = new StreamSource(new StringReader(xmlmessage));
//
//            jaxbContext = JAXBContext.newInstance(MqMsgDto.class);
//            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
//
//            mqMsgDto = (MqMsgDto) jaxbUnmarshaller.unmarshal(streamSource);
//
//            return mqMsgDto;
//
//        } catch (JAXBException e) {
//            mqMsgDto = new MqMsgDto();
//            e.printStackTrace();
//            return null;
//        }
//    }
//}
//
