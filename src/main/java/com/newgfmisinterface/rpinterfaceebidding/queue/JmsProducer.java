//package com.newgfmisinterface.rpinterfaceebidding.queue;
//
//import javax.jms.JMSException;
//import javax.jms.Message;
//
//import com.newgfmisinterface.rpinterfaceebidding.utils.AES;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.jms.core.JmsTemplate;
//import org.springframework.jms.core.MessagePostProcessor;
//import org.springframework.stereotype.Component;
//
////import com.javasampleapproach.activemq.model.Product;
//
//@Component
//public class JmsProducer {
//    private static final Logger LOGGER = LoggerFactory.getLogger(JmsProducer.class);
//
//    @Autowired
//    JmsTemplate jmsTemplate;
//
//    @Value("${spring.activemq.queue.producer}")
//    private String queueProducer;
//
//    private AES aes = new AES();
//
//    public void send(String message) {
//        String messageMQ = aes.encrypt(message, "123456789");
//        LOGGER.info("sending message='{}'", messageMQ);
////        	jmsTemplate.setTimeToLive(3000);
//        jmsTemplate.convertAndSend(queueProducer, messageMQ);
////        jmsTemplate.convertAndSend("helloworld.q", messageMQ, m -> {
////            m.setLongProperty(ScheduledMessage.AMQ_SCHEDULED_DELAY, 100000);
////            return m;
////        });
//    }
//}
