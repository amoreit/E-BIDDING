package com.newgfmisinterface.rpinterfaceebidding.schedule;

import com.newgfmisinterface.rpinterfaceebidding.dao.*;
import com.newgfmisinterface.rpinterfaceebidding.job.ProcessJobAMQ;
import com.newgfmisinterface.rpinterfaceebidding.job.ProcessJobEBD;
import com.newgfmisinterface.rpinterfaceebidding.job.ProcessJobSTM;
import com.newgfmisinterface.rpinterfaceebidding.service.FileStorageService;
import com.newgfmisinterface.rpinterfaceebidding.service.ScanFilePathService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@EnableScheduling
@Service
public class AnnotationScheduler {
    private static Logger LOGGER = LoggerFactory.getLogger(AnnotationScheduler.class);
    @Autowired
    private FileStorageService fileStorageService;
    @Autowired
    private ScanFilePathService scanFilePath;
    @Autowired
    private ProcessJobEBD processJobEBD;
    @Autowired
    private TaskScheduler taskScheduler;
    @Autowired
    private EbiddingErrorDao ebiddingErrorDao;
    @Autowired
    private ProcessJobAMQ processJobAMQ;
    @Autowired
    private SystemConfigDao systemConfigDao;
    @Autowired
    private ProcessJobSTM processJobSTM;
    @Autowired
    private EbiddingActiveMQDao ebiddingActiveMQDao;
    @Bean
    public String getConfigSchedule() {
        try {
            return this.systemConfigDao.getSchedulePeriodD();
        } catch (Exception e) {
            e.printStackTrace();
            return "0 0 20 * * ?";
        }
    }
    @Scheduled(cron = "#{@getConfigSchedule}" , zone = "GMT+7:00")
    public void runCron() {
        long startTimeTotal = System.currentTimeMillis();

        Runnable taskProcessSTM = () -> processJobSTM.jobRunner();
        taskScheduler.schedule(taskProcessSTM, new Date());

        String[] projectFile = {"GFEBIDINGRCRD", "GFEBIDINGR3R4"};
        List<String> projectName = new ArrayList<>();
        int lengthProject = projectFile.length;
        for (String projectCode : projectFile) {

            long startTime = System.currentTimeMillis();
            List<String> moveFile = scanFilePath.listFilesForFolder(new File(fileStorageService.getFileInputLocation().toString()), projectCode);

            int fileListForScan = 0;
            for (String moveFileToProcess : moveFile) {
                File fileName = new File(moveFileToProcess);
                String getFileName = fileName.getName();
                projectName.add(getFileName);
                fileStorageService.moveFileToProcessDir(moveFileToProcess);
                fileListForScan++;
            }
            if (fileListForScan > 0) {
                List<String> fileList = scanFilePath.listFilesForFolder(new File(fileStorageService.getFileStorageLocation().toString()), projectCode);
                for (String filename : fileList) {
                    processJobEBD.jobRunner(filename);
                }
            }
            lengthProject--;

            long endTime = System.currentTimeMillis();
            long duration = (endTime - startTime);
            System.out.print("Execution time is " + duration + " milli seconds\n");

            if (lengthProject == 0) {
                for (String projectname : projectName) {
                    processJobAMQ.jobRunner(projectname, 1);
                    try {
                        int checkLoop = ebiddingActiveMQDao.countItemFileError(projectname);
                        if (checkLoop > 0) {
                            for (int i = 1; i <= 2; i++) {
                                processJobAMQ.jobRunner(projectname, 2);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        long endTimeTotal = System.currentTimeMillis();
        long durationTotal = (endTimeTotal - startTimeTotal);
        System.out.print("Completed time is " + durationTotal + " milli seconds");

    }
}
