package com.newgfmisinterface.rpinterfaceebidding.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ParameterizedPreparedStatementSetter;
import org.springframework.stereotype.Repository;
import com.newgfmisinterface.rpinterfaceebidding.dto.EbiddingItemDto;

@Repository
public class EbiddingItemDao {
	private static Logger LOGGER = LoggerFactory.getLogger(EbiddingItemDao.class);
	private Executor executor = Executors.newFixedThreadPool(10);

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public CompletableFuture<int[][]> batchAdd(final Collection<EbiddingItemDto> ebiddingItemDtoList, int batchSize) {
		return CompletableFuture.supplyAsync(() -> {
			long startTime = System.currentTimeMillis();

			String sql = "INSERT INTO TH_RP_TRN_EBIDDING_ITEM (FILE_NAME, TRANS_DATE, DOC_TYPE, ITEMNO, CCT, CCT_OWNER, XREF3, ZZDEPOSIT, ZZOWNER, AMOUNT) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			int[][] updateCounts = jdbcTemplate.batchUpdate(
					sql,
					ebiddingItemDtoList,
					batchSize,
					new ParameterizedPreparedStatementSetter<EbiddingItemDto>() {
						public void setValues(PreparedStatement ps, EbiddingItemDto argument) throws SQLException {
							ps.setString(1, argument.getFilename());
							ps.setString(2, argument.getTransdate());
							ps.setString(3, argument.getDoctype());
							ps.setInt(4, argument.getItemno());
							ps.setString(5, argument.getCct());
							ps.setString(6, argument.getCctowner());
							ps.setString(7, argument.getXref3());
							ps.setString(8, argument.getZzdeposit());
							ps.setString(9, argument.getZzowner());
							ps.setString(10, argument.getAmount());
						}
					});

			long endTime = System.currentTimeMillis();
			long duration = (endTime - startTime);

//			System.out.println("Execution Insert  batchAdd  time is " + duration + " milli seconds - Batch Size "+ batchSize+ " Item");

			return updateCounts;

		}, executor).exceptionally(ex -> {
			LOGGER.error("Something went wrong : ", ex);
			return null;
		});

	}
	public int checkPaymentCenter(String paraMiter){
		int codeParams;
		String sql= "SELECT COUNT(*) AS COUNT FROM TH_BGPAYMENTCENTER WHERE VALUECODE=(SELECT c.TH_BGPAYMENTCENTER FROM TH_BGCOSTCENTER c WHERE c.VALUECODE='"+paraMiter+"') AND ISACTIVE='N' ";
		codeParams = jdbcTemplate.queryForObject(sql, Integer.class);
//System.out.println("<<< countCadepositAccount: " + codeParams);
		return codeParams;
	}
	public int countTransDate(String fileName,String transDate,String docType){
		int codeParams;
		String sql= "SELECT COUNT(*) AS COUNT FROM TH_RP_TRN_EBIDDING_ITEM WHERE FILE_NAME='"+fileName+"' AND TRANS_DATE='"+transDate+"' AND DOC_TYPE='"+docType+"' ";
		codeParams = jdbcTemplate.queryForObject(sql, Integer.class);
//System.out.println("<<< countCadepositAccount: " + codeParams);
		return codeParams;
	}
	public int countCadepositAccount(String paraMiter){
		int codeParams;
		String sql= "SELECT COUNT(*) AS COUNT FROM TH_CADEPOSITACCOUNT WHERE VALUECODE='"+paraMiter+"' AND ISACTIVE='N' ";
		codeParams = jdbcTemplate.queryForObject(sql, Integer.class);
//System.out.println("<<< countCadepositAccount: " + codeParams);
		return codeParams;
	}
	public int countPaymentCenter(String paraMiter){
		int codeParams;
		String sql= "SELECT COUNT(*) AS COUNT FROM TH_BGPAYMENTCENTER WHERE VALUECODE='"+paraMiter+"' AND ISACTIVE='N' ";
		codeParams = jdbcTemplate.queryForObject(sql, Integer.class);
//System.out.println("<<< countPaymentCenter: " + codeParams);
		return codeParams;
	}
	public int countCostcenter(String paraMiter){
		int codeParams;
		String sql1= "SELECT COUNT(*) AS COUNT FROM TH_BGCOSTCENTER WHERE  VALUECODE='"+paraMiter+"' AND ISACTIVE='N' ";
		codeParams = jdbcTemplate.queryForObject(sql1, Integer.class);
//System.out.println("<<< countTrdmCode: " + codeParams);
		return codeParams;
	}
	public int countPaymentcenter(String paraMiter, String paraMiter2){
		int codeParams;
		String sql1= "SELECT COUNT(*) AS COUNT FROM TH_BGCOSTCENTER WHERE TH_BGFUNDCENTER='"+paraMiter+"' AND TH_BGPAYMENTCENTER='"+paraMiter2+"' AND ISACTIVE='N' ";
		codeParams = jdbcTemplate.queryForObject(sql1, Integer.class);
//System.out.println("<<< countTrdmCode: " + codeParams);
		return codeParams;
	}
	public int truncate() throws Exception {
		String sql = "TRUNCATE TABLE TH_RP_TRN_EBIDDING_ITEM";
		return jdbcTemplate.update(sql);
	}

}
