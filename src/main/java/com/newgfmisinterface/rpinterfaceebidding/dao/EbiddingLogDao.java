package com.newgfmisinterface.rpinterfaceebidding.dao;

import com.newgfmisinterface.rpinterfaceebidding.dto.EbiddingLogDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class EbiddingLogDao {
	private static Logger LOGGER = LoggerFactory.getLogger(EbiddingErrorDao.class);
	@Autowired
	private JdbcTemplate jdbcTemplate;

	public int countLogSame(String filename,String date,String cocType){
		int codeParams;
		String sql= "SELECT COUNT(*) AS COUNT FROM TH_RP_TRN_EBIDDING_LOG WHERE FILE_NAME = '"+filename+"' AND TRANS_DATE= '"+date+"' AND DOC_TYPE= '"+cocType+"' AND STATUS='P' ";
		codeParams = jdbcTemplate.queryForObject(sql, Integer.class);
//System.out.println("<<< countCadepositAccount: " + codeParams);
		return codeParams;
	}
	public int countCheckStatusInsert(String filename){
		int codeParams;
		String sql= "SELECT COUNT(*) AS COUNT FROM TH_RP_TRN_EBIDDING_LOG WHERE FILE_NAME = '"+filename+"' AND STATUS='P' ";
		codeParams = jdbcTemplate.queryForObject(sql, Integer.class);
//System.out.println("<<< countCadepositAccount: " + codeParams);
		return codeParams;
	}
	public int updateLogCode(EbiddingLogDto dto,String dateTime) throws Exception {
		String sql = "UPDATE TH_RP_TRN_EBIDDING_LOG SET TOT_AMT ="+dto.getTotamt()+",TOT_REC = "+dto.getTotrec()+",POST_REC ="+dto.getPostrec()+",ERR_REC = "+dto.getErrrec()+",UPLOAD_FINISH = TO_DATE('"+ dto.getUploadfinish()+"','DD-MON-YY HH24:MI:SS') ,STATUS='"+dto.getStatus()+"' " +
				"WHERE FILE_NAME = '"+dto.getFilename()+"' " +
				"AND TRANS_DATE= '"+dto.getTransdate()+"' " +
				"AND DOC_TYPE= '"+dto.getDoctype()+"' " +
				"AND UPLOAD_START= TO_DATE('"+dateTime+"','DD-MON-YY HH24:MI:SS') " +
				"AND STATUS='C'";
//System.out.println("<<< sql-updateLogCode: " + sql);
		return jdbcTemplate.update(sql);
	}
	public int updateLogStatusError(String filename,String date) throws Exception {
		String sql = "UPDATE TH_RP_TRN_EBIDDING_LOG SET STATUS='E',POST_REC='0' WHERE FILE_NAME = '"+filename+"' AND TRANS_DATE= '"+date+"' AND STATUS in ('F','A')";
//System.out.println("<<< sql-updateLogStatusError: " + sql);
		return jdbcTemplate.update(sql);
	}
	public int updateLogStatusPost(String filename,String date) throws Exception {
		String sql = "UPDATE TH_RP_TRN_EBIDDING_LOG SET STATUS='P' WHERE FILE_NAME = '"+filename+"' AND TRANS_DATE= '"+date+"' AND STATUS= 'A'";
//System.out.println("<<< sql-updateLogCode: " + sql);
		return jdbcTemplate.update(sql);
	}
	public int addLog(EbiddingLogDto dto) throws Exception {
		String sql ="INSERT INTO TH_RP_TRN_EBIDDING_LOG (FILE_NAME, TRANS_DATE, DOC_TYPE, STATUS, TOT_AMT, " +
				 	"TOT_REC, ERR_REC, POST_REC, UPLOAD_START,UPLOAD_FINISH)" +
				 	"values ('"+dto.getFilename()+"', '"+dto.getTransdate()+"', '"+dto.getDoctype()+"', '"+dto.getStatus()+"', "+dto.getTotamt()+"," +
					" "+dto.getTotrec()+", "+dto.getErrrec()+", "+dto.getPostrec()+", TO_DATE('"+dto.getUploadstart()+"','DD-MON-YY HH24:MI:SS'), TO_DATE('"+dto.getUploadstart()+"','DD-MON-YY HH24:MI:SS'))";
//System.out.println("file => " + sql);
		return jdbcTemplate.update(sql);
	}

	public int truncate() throws Exception {
		String sql = "TRUNCATE TABLE TH_RP_TRN_EBIDDING_LOG";
		return jdbcTemplate.update(sql);
	}

}
