package com.newgfmisinterface.rpinterfaceebidding.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import com.newgfmisinterface.rpinterfaceebidding.dto.EbiddingStatementDto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class EbiddingStatementDao {
    private static Logger LOGGER = LoggerFactory.getLogger(EbiddingStatementDao.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public int countInsert(String fileName){
        int codeParams;
        String sql= "SELECT COUNT(TRANS_FILE_NAME) AS COUNT1 FROM TH_RP_TRN_EBIDDING_STATEMENT WHERE TRANS_FILE_NAME='"+fileName+"'";
        codeParams = jdbcTemplate.queryForObject(sql, Integer.class);
//System.out.println("<<< countInsert: " + codeParams);
        return codeParams;
    }
    public int countCheckTestAndProcess(String fileName){
        int codeParams;
        String sql= "SELECT COUNT(TRANS_FILE_NAME) AS COUNT1 FROM TH_RP_TRN_EBIDDING_STATEMENT WHERE TRANS_FILE_NAME='"+fileName+"' AND RECORD_STATUS='T'";
        codeParams = jdbcTemplate.queryForObject(sql, Integer.class);
//System.out.println("<<< countInsert: " + codeParams);
        return codeParams;
    }
    public int add(EbiddingStatementDto dto,String transDate,String uploadDate,String fileName) throws Exception {
        String sql = "INSERT INTO TH_RP_TRN_EBIDDING_STATEMENT (TRANS_FILE_NAME,TRANS_DATE,UPLOAD_DATE,TOTAL_AMT,AMOUNT01,AMOUNT02,AMOUNT03,AMOUNT04,AMOUNT05) " +
                "values ('"+fileName+"','"+transDate+"', TO_DATE('"+uploadDate+"','DD-MON-YY HH24:MI:SS'), " +
                "TO_NUMBER("+dto.getTotalamt()+",'9999999999.99'), " +
                "TO_NUMBER("+dto.getAmount01()+",'9999999999.99') , " +
                "TO_NUMBER("+dto.getAmount02()+",'9999999999.99') , " +
                "TO_NUMBER("+dto.getAmount03()+",'9999999999.99') , " +
                "TO_NUMBER("+dto.getAmount04()+",'9999999999.99'), " +
                "TO_NUMBER("+dto.getAmount05()+",'9999999999.99'))";
//System.out.println("add>>>>>>>>>>>>>>"+sql);
        return jdbcTemplate.update(sql);
    }
    public int testProcess(EbiddingStatementDto dto,String transDate,String uploadDate,String fileName,String status) throws Exception {
        String sql = "INSERT INTO TH_RP_TRN_EBIDDING_STATEMENT (RECORD_STATUS,TRANS_FILE_NAME,TRANS_DATE,UPLOAD_DATE,TOTAL_AMT,AMOUNT01,AMOUNT02,AMOUNT03,AMOUNT04,AMOUNT05) " +
                "values ('"+status+"','"+fileName+"','"+transDate+"', TO_DATE('"+uploadDate+"','DD-MON-YY HH24:MI:SS'), " +
                "TO_NUMBER("+dto.getTotalamt()+",'9999999999.99'), " +
                "TO_NUMBER("+dto.getAmount01()+",'9999999999.99') , " +
                "TO_NUMBER("+dto.getAmount02()+",'9999999999.99') , " +
                "TO_NUMBER("+dto.getAmount03()+",'9999999999.99') , " +
                "TO_NUMBER("+dto.getAmount04()+",'9999999999.99'), " +
                "TO_NUMBER("+dto.getAmount05()+",'9999999999.99'))";
//System.out.println("add>>>>>>>>>>>>>>"+sql);
        return jdbcTemplate.update(sql);
    }
    public int update(EbiddingStatementDto dto,String uploadDate,String fileName) throws Exception {
        String sql = "UPDATE TH_RP_TRN_EBIDDING_STATEMENT " +
                "SET TOTAL_AMT=TO_NUMBER("+dto.getTotalamt()+",'9999999999.99')," +
                "UPLOAD_DATE=TO_DATE('"+uploadDate+"','DD-MON-YY HH24:MI:SS')," +
                "AMOUNT01=TO_NUMBER("+dto.getAmount01()+",'9999999999.99')," +
                "AMOUNT02=TO_NUMBER("+dto.getAmount02()+",'9999999999.99')," +
                "AMOUNT03=TO_NUMBER("+dto.getAmount03()+",'9999999999.99')," +
                "AMOUNT04=TO_NUMBER("+dto.getAmount04()+",'9999999999.99')," +
                "AMOUNT05=TO_NUMBER("+dto.getAmount05()+",'9999999999.99') " +
                " WHERE TRANS_FILE_NAME = '"+fileName+"' ";
//System.out.println("<<< sql-updateLogCode: " + sql);
        return jdbcTemplate.update(sql);
    }
    public List<Map<String, Object>> updateProcess(String fileName) throws Exception {
        List<Map<String, Object>> resultList = new ArrayList<>();
        String sql = "UPDATE TH_RP_TRN_EBIDDING_STATEMENT SET RECORD_STATUS='P' WHERE TRANS_FILE_NAME = '"+fileName+"' ";
        jdbcTemplate.update(sql);

        Map<String, Object> mapFileName = new HashMap<>();
        mapFileName.put("fileName", fileName);
        resultList.add(mapFileName);
        Map<String, Object> mapErrorId1 = new HashMap<>();
        mapErrorId1.put("errorId", 0);
        resultList.add(mapErrorId1);
        Map<String, Object> mapErrorId2 = new HashMap<>();
        mapErrorId2.put("errorId", 0);
        resultList.add(mapErrorId2);
        Map<String, Object> mapErrorId3 = new HashMap<>();
        mapErrorId3.put("errorId", 0);
        resultList.add(mapErrorId3);
        Map<String, Object> mapErrorId4 = new HashMap<>();
        mapErrorId4.put("errorId", 0);
        resultList.add(mapErrorId4);
        Map<String, Object> mapErrorId5 = new HashMap<>();
        mapErrorId5.put("errorId", 0);
        resultList.add(mapErrorId5);
        Map<String, Object> mapStatus = new HashMap<>();
        mapStatus.put("statusFile", "PASS");
        resultList.add(mapStatus);
        return resultList;
    }
    public int sumAmount(String date,String dateFile,String type){
        String fileName = "GSTMTEBIDING"+dateFile+".TXT";
        int sumAmount;
        String totalAmount = null;
        switch(type) {
            case "RC":
                totalAmount = "AMOUNT01";
                break;
            case "RD":
                totalAmount = "AMOUNT02";
                break;
            case "R3":
                totalAmount = "AMOUNT03";
                break;
            case "R4":
                totalAmount = "AMOUNT04";
                break;
        }
        String sql = "SELECT NVL(SUM("+totalAmount+"),0) nvl_sum FROM TH_RP_TRN_EBIDDING_STATEMENT WHERE TRANS_DATE ='"+date+"' AND TRANS_FILE_NAME = '"+fileName+"'";
//System.out.println("<<< sql-sumAmount: " + sql);
        sumAmount = jdbcTemplate.queryForObject(sql, Integer.class);

        return sumAmount;
    }
    public int StatusInsertStatement(String filename){
        int codeParams;
        String sql= "SELECT COUNT(*) AS COUNT FROM TH_RP_TRN_EBIDDING_STATEMENT WHERE TRANS_FILE_NAME = '"+filename+"' AND RECORD_STATUS='P' ";
        codeParams = jdbcTemplate.queryForObject(sql, Integer.class);
//System.out.println("<<< countCadepositAccount: " + codeParams);
        return codeParams;
    }
    public int truncate() throws Exception {
        String sql = "TRUNCATE TABLE TH_RP_TRN_EBIDDING_STATEMENT";
        return jdbcTemplate.update(sql);
    }
}
