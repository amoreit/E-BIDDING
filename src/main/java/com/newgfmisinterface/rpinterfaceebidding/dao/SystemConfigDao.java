package com.newgfmisinterface.rpinterfaceebidding.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class SystemConfigDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public String getInputFileConfig()  throws Exception  {
        String query = "SELECT VALUE FROM SYS_PARAMETERS_CONFIG WHERE PARAMETER_NAME='INPUT_PATH_EBIDDING_BATCHJOB'";
        return jdbcTemplate.queryForObject(query,new Object[]{},String.class);
    }
    public String getOutputFileConfig()  throws Exception  {
        String query = "SELECT VALUE FROM SYS_PARAMETERS_CONFIG WHERE PARAMETER_NAME='OUTPUT_PATH_EBIDDING_BATCHJOB'";
        return jdbcTemplate.queryForObject(query,new Object[]{},String.class);
    }
    public String getUploadFileConfig()  throws Exception  {
        String query = "SELECT VALUE FROM SYS_PARAMETERS_CONFIG WHERE PARAMETER_NAME='UPLOAD_PATH_EBIDDING_BATCHJOB'";
        return jdbcTemplate.queryForObject(query,new Object[]{},String.class);
    }
    public String getExtractFileConfig()  throws Exception  {
        String query = "SELECT VALUE FROM SYS_PARAMETERS_CONFIG WHERE PARAMETER_NAME='EXTRACT_PATH_EBIDDING_BATCHJOB'";
        return jdbcTemplate.queryForObject(query,new Object[]{},String.class);
    }
    public String getSuccessFileConfig()  throws Exception  {
        String query = "SELECT VALUE FROM SYS_PARAMETERS_CONFIG WHERE PARAMETER_NAME='SUCCESS_PATH_EBIDDING_BATCHJOB'";
        return jdbcTemplate.queryForObject(query,new Object[]{},String.class);
    }
    public String getErrorFileConfig()  throws Exception  {
        String query = "SELECT VALUE FROM SYS_PARAMETERS_CONFIG WHERE PARAMETER_NAME='ERROR_PATH_EBIDDING_BATCHJOB'";
        return jdbcTemplate.queryForObject(query,new Object[]{},String.class);
    }
    public String getSchedulePeriodD()  throws Exception  {
        String query = "SELECT VALUE FROM SYS_PARAMETERS_CONFIG WHERE PARAMETER_NAME='SCHEDULE_PERIODD_EBIDDING_BATCHJOB'";
        return jdbcTemplate.queryForObject(query,new Object[]{},String.class);
    }

}
