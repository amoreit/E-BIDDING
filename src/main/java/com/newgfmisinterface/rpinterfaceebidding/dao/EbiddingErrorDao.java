package com.newgfmisinterface.rpinterfaceebidding.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ParameterizedPreparedStatementSetter;
import org.springframework.stereotype.Repository;
import com.newgfmisinterface.rpinterfaceebidding.dto.EbiddingErrorDto;

@Repository
public class EbiddingErrorDao {
	private static Logger LOGGER = LoggerFactory.getLogger(EbiddingErrorDao.class);

	private Executor executor = Executors.newFixedThreadPool(10);

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public int add(EbiddingErrorDto dto) throws Exception {
		String sql = "INSERT INTO TH_RP_TRN_EBIDDING_ERROR (FILE_NAME,TRANS_DATE, DOC_TYPE,ITEMNO, MSGNO, MSGID, MESSAGE,UPLOAD_NAME, UPLOAD_DATE) values (?, ?, ? , ? , ? , ?, ?, ? , ?)";
		return jdbcTemplate.update(sql, dto.getFilename(), dto.getTransdate(), dto.getDoctype(), dto.getItemno(), dto.getMsgno(),dto.getMsgid(), dto.getMessage(), dto.getUploadname(), dto.getUploaddate());
	}

	public CompletableFuture<int[][]> batchAdd(final Collection<EbiddingErrorDto> ebiddingErrorDtoList, int batchSize) {

		return CompletableFuture.supplyAsync(() -> {
			long startTime = System.currentTimeMillis();
			String sql = "INSERT INTO TH_RP_TRN_EBIDDING_ERROR (FILE_NAME,TRANS_DATE, DOC_TYPE,ITEMNO, MSGNO, MSGID, MESSAGE,UPLOAD_NAME, UPLOAD_DATE) values (?, ?, ? , ? , ? , ?, ?, ? , ?)";

			int[][] updateCounts = jdbcTemplate.batchUpdate(
					sql,
					ebiddingErrorDtoList,
					batchSize,
					new ParameterizedPreparedStatementSetter<EbiddingErrorDto>() {
						public void setValues(PreparedStatement ps, EbiddingErrorDto argument) throws SQLException {
							ps.setString(1, argument.getFilename());
							ps.setString(2, argument.getTransdate());
							ps.setString(3, argument.getDoctype());
							ps.setInt(4, argument.getItemno());
							ps.setString(5, argument.getMsgid());
							ps.setString(6, argument.getMsgid());
							ps.setString(7, argument.getMessage());
							ps.setString(8, argument.getUploadname());
							ps.setString(9, argument.getTransdate());
						}
					});
			long endTime = System.currentTimeMillis();
			long duration = (endTime - startTime);

//			System.out.println("Execution Insert  batchAdd  time is " + duration + " milli seconds - Batch Size "+ batchSize+ " Item");
			return updateCounts;

		}, executor).exceptionally(ex -> {
			LOGGER.error("Something went wrong : ", ex);
			return null;
		});
	}
	public int countError(String filename){
		int codeParams;
		String sql= "SELECT COUNT(*) AS COUNT1 FROM TH_RP_TRN_EBIDDING_LOG WHERE FILE_NAME = '"+filename+"' AND STATUS='F'";
		codeParams = jdbcTemplate.queryForObject(sql, Integer.class);
//System.out.println("<<< countError: " + sql1);
		return codeParams;
	}
	public int deleteItem(String filename) throws Exception {
		String sql = "DELETE FROM TH_RP_TRN_EBIDDING_ITEM WHERE FILE_NAME='"+filename+"'";
		return jdbcTemplate.update(sql);
	}
	public List<Map<String, Object>> Error(List<EbiddingErrorDto> listError) throws Exception {
		List<Map<String, Object>> resultList = new ArrayList<>();
		Map<String, Object> map = new HashMap<>();
		for (EbiddingErrorDto errorData : listError) {
			map.put("filename", errorData.getFilename());
			map.put("transdate", errorData.getTransdate());
			map.put("doctype", errorData.getDoctype());
			map.put("itemno", errorData.getItemno());
			map.put("message", errorData.getMessage());
			resultList.add(map);
//                System.out.println("map"+map);
                System.out.println("resultList"+resultList);
		}
		return resultList;
	}
//	public String getCountUpdateMSMQ(String filename, String date, String itemno, String doctype) throws SQLException {
//		String sqlCountUpdateMSMQ = "SELECT DOC_NO FROM TH_RP_TRN_EBIDDING_MSGQ_LOG WHERE TRANS_FILE_NAME='" + filename + "' AND SEND_DATE='" + date + "' AND TRANS_NO='" + itemno+ "'  AND DOC_TYPE='" + doctype + "'";
//		return jdbcTemplate.queryForObject(sqlCountUpdateMSMQ,new Object[]{},String.class);
//	}
	public int truncate() throws Exception {
		String sql = "TRUNCATE TABLE TH_RP_TRN_EBIDDING_ERROR";
		return jdbcTemplate.update(sql);
	}

}
