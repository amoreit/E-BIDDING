package com.newgfmisinterface.rpinterfaceebidding.dao;

import java.io.StringWriter;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import com.newgfmisinterface.rpinterfaceebidding.dto.MqMsgDto;
import com.newgfmisinterface.rpinterfaceebidding.dto.MqMsgReplyDto;
import com.newgfmisinterface.rpinterfaceebidding.dto.XmlData;
import com.newgfmisinterface.rpinterfaceebidding.queue.SenderQueue;
//import com.newgfmisinterface.rpinterfaceebidding.queue.JmsProducer;
import com.newgfmisinterface.rpinterfaceebidding.validate.TextEbiddingValidate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

@Repository
public class EbiddingActiveMQDao {
    private static Logger LOGGER = LoggerFactory.getLogger(EbiddingActiveMQDao.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private EbiddingErrorDao ebiddingErrorDao;

//    @Autowired
//    private JmsProducer jmsProducer;
    @Autowired
    private SenderQueue senderQueue;

    private TextEbiddingValidate textEbiddingValidate = new TextEbiddingValidate();
    private static AtomicInteger id = new AtomicInteger();

    public void getActiveQueue(XmlData xmlData, int countCode) throws JAXBException {

//        Integer myid = id.getAndIncrement();
        int itemno = Integer.parseInt(xmlData.getItemno());
        String dateTime = new SimpleDateFormat("dd-MMM-yy").format(new Date());
        String Date = new SimpleDateFormat("MMyy").format(new Date());
        String countMSMQ = null;

        int countUpdate = countCode+1;
        if(countCode <= 2) {
            if(countCode == 0) {
                String code = "SEQ-" + Date;
                int count;
                String sqlCount = "SELECT COUNT(SEND_SEQ) AS COUNT1 FROM TH_RP_TRN_EBIDDING_MSGQ_LOG WHERE SEND_SEQ LIKE '%" + code + "%' ";
                count = jdbcTemplate.queryForObject(sqlCount, Integer.class);
                System.out.println("<<< count : " + count);
                if (count < 9) {
                    countMSMQ = code + "/" + "00000000" + (count + 1);
                }else if(count < 99 && count >= 9){
                    countMSMQ = code + "/" + "0000000" + (count + 1);
                }else if(count < 999 && count >= 99){
                    countMSMQ = code + "/" + "000000" + (count + 1);
                }else if(count < 9999 && count >= 999){
                    countMSMQ = code + "/" + "00000" + (count + 1);
                }else if(count < 99999 && count >= 9999){
                    countMSMQ = code + "/" + "0000" + (count + 1);
                }else if(count < 999999 && count >= 99999){
                    countMSMQ = code + "/" + "000" + (count + 1);
                }else if(count < 9999999 && count >= 999999){
                    countMSMQ = code + "/" + "00" + (count + 1);
                }else if(count < 99999999 && count >= 9999999){
                    countMSMQ = code + "/" + "0" + (count + 1);
                }else if(count < 999999999 && count >= 99999999){
                    countMSMQ = code + "/" + (count + 1);
                }
//System.out.println("<<< countMSMQ  : " + countMSMQ);
                String sql = "INSERT INTO TH_RP_TRN_EBIDDING_MSGQ_LOG (SEND_SEQ,SEND_DATE,TRANS_SEQ,TRANS_NO,TRANS_FILE_NAME,DOC_TYPE,SEND_LOOP,IS_SEND) values ('" + countMSMQ + "','"+dateTime+"'," + itemno + "," + itemno + ",'" + xmlData.getFilename() + "','"+xmlData.getDoctype()+"','1','Y')";
//System.out.println("<<< INSERT sql : " + sql);
                jdbcTemplate.update(sql);
            }else if(countCode > 0 && countCode <= 2) {
                String sql = "UPDATE TH_RP_TRN_EBIDDING_MSGQ_LOG SET SEND_LOOP='"+countUpdate+"' WHERE TRANS_FILE_NAME='"+xmlData.getFilename()+"' AND TRANS_NO='"+itemno+"' AND DOC_TYPE='"+xmlData.getDoctype()+"'";
                System.out.println("<<< UPDATE sql : " + sql);
                jdbcTemplate.update(sql);

                String sqlCountUpdateMSMQ = "SELECT NVL(SEND_SEQ,'0') AS SEND_SEQ FROM TH_RP_TRN_EBIDDING_MSGQ_LOG WHERE TRANS_FILE_NAME='" + xmlData.getFilename() + "'  AND TRANS_NO='" + xmlData.getItemno() + "'  AND DOC_TYPE='" + xmlData.getDoctype() + "'";
                String countUpdateMSMQ = (String) jdbcTemplate.queryForObject(sqlCountUpdateMSMQ, String.class);
                countMSMQ = countUpdateMSMQ;
//                System.out.println("<<< countUpdateMSMQ: " + countUpdateMSMQ);
            }

            MqMsgDto mqMsgXml = new MqMsgDto();
            mqMsgXml.setId(countMSMQ);
            XmlData data = new XmlData(xmlData.getFilename(), xmlData.getTransdate(), xmlData.getDoctype(), xmlData.getItemno(), xmlData.getCct(), xmlData.getCctowner(), xmlData.getXref3(), xmlData.getZzdeposit(), xmlData.getZzowner(), xmlData.getAmount());
            mqMsgXml.setData(data);

            JAXBContext jaxbContext = JAXBContext.newInstance(MqMsgDto.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            StringWriter sw = new StringWriter();
            marshaller.marshal(mqMsgXml, sw);
            String xmlContent = sw.toString();

            senderQueue.send(xmlContent);
        }
    }
//    public List<Map<String, Object>> getCountUpdateMSMQ(String filename, String date, String itemno, String doctype) throws Exception {
//        List<Map<String, Object>> resultList = null;
//        String sql = "SELECT DOC_NO FROM TH_RP_TRN_EBIDDING_MSGQ_LOG WHERE TRANS_FILE_NAME='" + filename + "' AND SEND_DATE='" + date + "' AND TRANS_NO='" + itemno+ "'  AND DOC_TYPE='" + doctype + "'";
////        return jdbcTemplate.queryForList(sqlCountUpdateMSMQ);
//        resultList = jdbcTemplate.queryForList(sql);
//        return resultList;
//    }

    public int countItemFileName(String fileName){
        int codeParams;
        String sql= "SELECT COUNT(*) AS COUNT FROM TH_RP_TRN_EBIDDING_ITEM WHERE FILE_NAME='"+fileName+"'";
        codeParams = jdbcTemplate.queryForObject(sql, Integer.class);
//System.out.println("<<< countCadepositAccount: " + codeParams);
        return codeParams;
    }
//    public List<Map<String, Object>> countLoop(XmlData xmlData){
//        List<Map<String, Object>> resultList = null;
//        int itemno = Integer.parseInt(xmlData.getItemno());
//        String sql= "SELECT SEND_LOOP FROM TH_RP_TRN_EBIDDING_MSGQ_LOG WHERE TRANS_FILE_NAME='"+xmlData.getFilename()+"' AND TRANS_NO='"+itemno+"' AND DOC_TYPE='"+xmlData.getDoctype()+"'";
//        resultList = jdbcTemplate.queryForList(sql);
//System.out.println("<<< countLoop: " + sql);
//        return resultList;
//    }
public int countLoop(XmlData xmlData){
    int count = 0;
    int itemno = Integer.parseInt(xmlData.getItemno());
    String sql= "SELECT NVL(SUM(SEND_LOOP),0) nvl_sum  FROM TH_RP_TRN_EBIDDING_MSGQ_LOG WHERE TRANS_FILE_NAME='"+xmlData.getFilename()+"' AND TRANS_NO='"+itemno+"' AND DOC_TYPE='"+xmlData.getDoctype()+"'";
//System.out.println("<<< countLoop: " + sql);
    int countLoop = jdbcTemplate.queryForObject(sql, Integer.class);
//    countLoop = countLoop+count;
//    int countLoop1 = countLoop == 0 ? 0 : countLoop;
    return countLoop;
}
    public int countItemFileError(String fileName){
        String dateTime = new SimpleDateFormat("dd-MMM-yy").format(new Date());
        int codeParams;
        String sql= "SELECT COUNT(*) AS COUNT FROM TH_RP_TRN_EBIDDING_MSGQ_LOG WHERE TRANS_FILE_NAME='"+fileName+"' AND SEND_DATE='"+dateTime+"' AND SEND_LOOP >= 1 AND DOC_NO IS NULL";
        codeParams = jdbcTemplate.queryForObject(sql, Integer.class);
//System.out.println("<<< countCadepositAccount: " + codeParams);
        return codeParams;
    }
    public List<Map<String, Object>> getEbiddingList(String fileName,String docType) throws Exception {
        List<Map<String, Object>> resultList = null;
        String sql = "SELECT FILE_NAME, TRANS_DATE, DOC_TYPE, ITEMNO, CCT, CCT_OWNER, XREF3, ZZDEPOSIT, ZZOWNER, AMOUNT FROM TH_RP_TRN_EBIDDING_ITEM WHERE FILE_NAME='"+fileName+"' AND DOC_TYPE='"+docType+"'";
//System.out.println(sql);
        resultList = jdbcTemplate.queryForList(sql);
        return resultList;
    }
    public List<Map<String, Object>> getEbiddingListMultipleLoop(String fileName,String docType) throws Exception {
        List<Map<String, Object>> resultList = null;
        String sql = "SELECT EI.FILE_NAME, EI.TRANS_DATE, " +
                "EI.DOC_TYPE, EI.ITEMNO, " +
                "EI.CCT, EI.CCT_OWNER, " +
                "EI.XREF3, EI.ZZDEPOSIT," +
                "EI.ZZOWNER, EI.AMOUNT  " +
                "FROM TH_RP_TRN_EBIDDING_ITEM  EI " +
                "INNER JOIN TH_RP_TRN_EBIDDING_MSGQ_LOG MQ ON MQ.TRANS_FILE_NAME=EI.FILE_NAME AND MQ.TRANS_NO=EI.ITEMNO AND MQ.DOC_TYPE=EI.DOC_TYPE " +
                "WHERE EI.FILE_NAME='"+fileName+"'AND EI.DOC_TYPE='"+docType+"' AND MQ.DOC_NO IS NULL AND MQ.SEND_LOOP >= 1 ORDER BY MQ.SEND_ID";
// System.out.println("getEbiddingListMultipleLoop"+sql);
        resultList = jdbcTemplate.queryForList(sql);
        return resultList;
    }
    public int updateLogMQCode(MqMsgReplyDto mqMsgReplyDto) throws Exception {
        String sql = "UPDATE TH_RP_TRN_EBIDDING_MSGQ_LOG SET DOC_NO='"+mqMsgReplyDto.getData().getDocno()+"' WHERE TRANS_FILE_NAME = '"+mqMsgReplyDto.getData().getFilename()+"' AND SEND_SEQ= '"+mqMsgReplyDto.getId()+"' AND TRANS_NO='"+mqMsgReplyDto.getData().getItemno()+"'";
//System.out.println("<<< updateLogMQCode: " + sql);
        return jdbcTemplate.update(sql);
    }
    public int countPostStart(String fileName,String type){
        String sql= "SELECT COUNT(*) AS COUNT FROM TH_RP_TRN_EBIDDING_LOG WHERE FILE_NAME='"+fileName+"' AND DOC_TYPE= '"+type+"' AND STATUS= 'P' AND POST_START  IS NULL";
        int codeParams = jdbcTemplate.queryForObject(sql, Integer.class);
System.out.println("<<< countPostStart: " + codeParams);
        return codeParams;
    }
    public int updateLogActiveMQStartPost(String filename,String type,String dateStart) throws Exception {
//        String formatStartDate = new SimpleDateFormat("dd-MMM-yy HH:mm:ss").format(new Date());
        String sql = "UPDATE TH_RP_TRN_EBIDDING_LOG SET POST_START=TO_DATE('"+ dateStart+"','DD-MON-YY HH24:MI:SS') WHERE FILE_NAME = '"+filename+"' AND DOC_TYPE= '"+type+"' AND STATUS= 'P'";
System.out.println("<<< sql-updateLogActiveMQStartPost: " + sql);
        return jdbcTemplate.update(sql);
    }
    public int updateLogActiveMQEndPost(String filename,String type) throws Exception {
        String sqlPostStart = "SELECT POST_START FROM TH_RP_TRN_EBIDDING_LOG WHERE TFILE_NAME = '"+filename+"' AND DOC_TYPE= '"+type+"' AND STATUS= 'P'";
        String startDate = (String) jdbcTemplate.queryForObject(sqlPostStart, String.class);

        String formatStartDate = new SimpleDateFormat("dd-MMM-yy HH:mm:ss").format(startDate);
        String formatEndDate = new SimpleDateFormat("dd-MMM-yy HH:mm:ss").format(new Date());
        String sql = "UPDATE TH_RP_TRN_EBIDDING_LOG SET POST_FINISH=TO_DATE('"+formatEndDate+"','DD-MON-YY HH24:MI:SS') WHERE FILE_NAME = '"+filename+"' AND DOC_TYPE= '"+type+"' AND STATUS= 'P' AND POST_START=TO_DATE('"+ formatStartDate+"','DD-MON-YY HH24:MI:SS')";
System.out.println("<<< sql-updateLogActiveMQEndPost: " + sql);
        return jdbcTemplate.update(sql);
    }
    public ArrayList<List<Map<String, Object>>> jobRunnerPostingFileActiveMq(String filename, String transDate,String docType) throws Exception {
        ArrayList<List<Map<String, Object>>> resultObject = new ArrayList<>();
        List<Map<String, Object>> resultList = new ArrayList<>();
        String dateForFile = textEbiddingValidate.dateFormatFileText(transDate);
        String docType1 = docType.substring(0, 2);
        String docType2 = docType.substring(2, 4);
        String [] loopDocType ={docType1,docType2};

        for(String type : loopDocType){
            String sql ="SELECT * FROM (SELECT (SELECT COUNT(1) FROM TH_RP_TRN_EBIDDING_ITEM T2 WHERE T2.FILE_NAME = '"+filename+"' AND T2.DOC_TYPE= '"+type+"' AND T2.TRANS_DATE ='"+dateForFile+"' ) AS ROW_TOTAL, ROW_NUMBER() OVER (ORDER BY T.TRN_EBIDDING_ITEM_ID) AS ROW_NUMBER, T.* FROM TH_RP_TRN_EBIDDING_ITEM T WHERE T.FILE_NAME = '"+filename+"' AND T.DOC_TYPE= '"+type+"' AND T.TRANS_DATE ='"+dateForFile+"' ORDER BY T.TRN_EBIDDING_ITEM_ID)  ORDER BY ROW_NUMBER ";
            System.out.println("<<< sql-jobRunnerPostingFileActiveMq: " + sql);
            resultList = jdbcTemplate.queryForList(sql);
            resultObject.add(resultList);
        }
        System.out.println("<<< sql-resultObject: " + resultObject);

        return resultObject;
    }
    public int truncate() throws Exception {
        String sql = "TRUNCATE TABLE TH_RP_TRN_EBIDDING_MSGQ_LOG";
        return jdbcTemplate.update(sql);
    }
}
