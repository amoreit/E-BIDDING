package com.newgfmisinterface.rpinterfaceebidding.job;

import com.newgfmisinterface.rpinterfaceebidding.dao.EbiddingErrorDao;
import com.newgfmisinterface.rpinterfaceebidding.dao.EbiddingItemDao;
import com.newgfmisinterface.rpinterfaceebidding.dao.EbiddingLogDao;
import com.newgfmisinterface.rpinterfaceebidding.dao.EbiddingStatementDao;
import com.newgfmisinterface.rpinterfaceebidding.dto.*;
import com.newgfmisinterface.rpinterfaceebidding.service.CheckErrorService;
import com.newgfmisinterface.rpinterfaceebidding.service.ErrorInsertService;
import com.newgfmisinterface.rpinterfaceebidding.service.FileStorageService;
import com.newgfmisinterface.rpinterfaceebidding.service.ScanFilePathService;
import com.newgfmisinterface.rpinterfaceebidding.validate.TextEbiddingCheck;
import com.newgfmisinterface.rpinterfaceebidding.validate.TextEbiddingValidate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Component
public class ProcessJobCAP {
    private static Logger LOGGER = LoggerFactory.getLogger(ProcessJobCAP.class);

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private ScanFilePathService scanFilePath;

    @Autowired
    private EbiddingLogDao ebiddingLogDao;

    @Autowired
    private EbiddingErrorDao ebiddingErrorDao;

    @Autowired
    private EbiddingItemDao ebiddingItemDao;

    @Autowired
    private EbiddingStatementDao ebiddingStatementDao;

    @Autowired
    private TextEbiddingCheck textEbiddingCheck;

    @Autowired
    private CheckErrorService checkErrorService;

    private TextEbiddingValidate textEbiddingValidate = new TextEbiddingValidate();

    public ArrayList<List<Map<String, Object>>> jobRunnerConvertFile(String filename,String transDate) throws Exception {
        List<String> fileInOutput = scanFilePath.listFilesOutputForFolder(new File(fileStorageService.getFileOutputLocation().toString()), filename);
        if(fileInOutput.size() >= 1){
            fileStorageService.deleteFileSuccess(filename);
        }
        List<String> fileTempList = fileStorageService.extractFileLine(filename);

        ArrayList<List<Map<String, Object>>> resultObject = new ArrayList<>();
        List<Map<String, Object>> resultListLog = new ArrayList<>();
        List<Map<String, Object>> resultList = new ArrayList<>();
        List<EbiddingErrorDto> listEbiddingError = new ArrayList<>();
        for (String fileProcess : fileTempList) {
            List<String> fileByline = fileStorageService.readFilebyLine(fileProcess);
            EbiddingHeaderItemDto headerLine = textEbiddingValidate.splitLineHeader(fileByline.get(fileByline.size() - fileByline.size()));
            EbiddingFooterItemDto endLine = textEbiddingValidate.splitLineFooter(fileByline.get(fileByline.size() - 1));

            // insert start process log to database
            File fileName = new File(fileProcess);
            String getFileName = fileName.getName();
            String ebiddingDocType = getFileName.substring(9, 11);
            String dateCode = filename.substring(13, 21);
            String newDateString = transDate;
            String dateString = textEbiddingValidate.dateFormat(dateCode);
            Integer totalRecordError = 0;
            double totalAmount = (double) 0;
            Map<String, Object> mapLog = new HashMap<>();
                // check error Excel Number 01
                int countTransDate = ebiddingItemDao.countTransDate(filename, newDateString, ebiddingDocType);
                if (countTransDate >= 1) {
                    EbiddingItemDto dataObj = new EbiddingItemDto();
                    dataObj.setItemno(0);
                    Integer errorNo = 1;

                    EbiddingErrorDto parseError = ErrorInsertService.errorPasser(dataObj, filename, newDateString, ebiddingDocType, errorNo);
                    listEbiddingError.add(parseError);

                    totalRecordError++;
                }
                // check error Excel Number 03
                if ((fileByline.size() - 2) <= 0) {
                    EbiddingItemDto dataObj = new EbiddingItemDto();
                    dataObj.setItemno(0);
                    Integer errorNo = 3;

                    EbiddingErrorDto parseError = ErrorInsertService.errorPasser(dataObj, filename, newDateString, ebiddingDocType, errorNo);
                    listEbiddingError.add(parseError);

                    totalRecordError++;
                }
                EbiddingFooterItemDto endLinedata = textEbiddingValidate.splitLineFooter(fileByline.get(fileByline.size() - 1));
                List<EbiddingItemDto> listEbiddingItemCheckPass = new ArrayList<>();
                List<CompletableFuture<EbiddingCheckDto>> checkResultAllList = new ArrayList<>();
                int row = 0;
                for (String line : fileByline) {
                    boolean haveErrorCheck = false;
                    row = row + 1;
                    // skip first line and end line
                    if (row == 1 || row == fileByline.size()) {
                        continue;
                    }
                    CompletableFuture<EbiddingCheckDto> resultFutureError = checkErrorService.checkProcess(line, ebiddingDocType, row, headerLine, endLine);
                    checkResultAllList.add(resultFutureError);
                }
                //End For Text line
                for (Future<EbiddingCheckDto> futureError : checkResultAllList) {
                    try {
                        EbiddingCheckDto resultEbiddingCheck = futureError.get();

                        boolean haveError = resultEbiddingCheck.isHaveError();
                        EbiddingItemDto ebiddingItemDto = resultEbiddingCheck.getEbiddingItemDto();
                        totalAmount = totalAmount + convertStringToDouble(ebiddingItemDto.getAmount());

                        if (haveError) {
                            List<Integer> errorNumberList = resultEbiddingCheck.getErrorList();
                            int numberOfError = errorNumberList.size();
                            totalRecordError = totalRecordError + numberOfError;

                            for (int errorNumber : errorNumberList) {
                                EbiddingErrorDto parseError = ErrorInsertService.errorPasser(ebiddingItemDto, filename, newDateString, ebiddingDocType, errorNumber);
                                listEbiddingError.add(parseError);
                            }
                        } else {
                            listEbiddingItemCheckPass.add(ebiddingItemDto);
                        }
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                    }
                }
                checkResultAllList.clear();
                fileByline.clear();
                // check error Excel Number 05
                if (!textEbiddingValidate.validateTotalAmount(totalAmount, endLinedata)) {
                    EbiddingItemDto dataObj = new EbiddingItemDto();
                    dataObj.setItemno(9999999);
                    Integer errorNo = 5;

                    EbiddingErrorDto parseError = ErrorInsertService.errorPasser(dataObj, filename, newDateString, ebiddingDocType, errorNo);
                    listEbiddingError.add(parseError);

                    totalRecordError++;
                }
                //check error Excel Number 12
                int sumAmountStatement = ebiddingStatementDao.sumAmount(dateString,dateCode,ebiddingDocType);
                Integer objAmountSTM = new Integer(sumAmountStatement);
                int valueTotalAmount1 = (int) totalAmount;
                Integer objAmountEBD = new Integer(valueTotalAmount1);

                if (!objAmountSTM.equals(objAmountEBD)) {
                    EbiddingItemDto dataObj = new EbiddingItemDto();
                    dataObj.setItemno(0);
                    Integer errorNo = 12;

                    EbiddingErrorDto parseError = ErrorInsertService.errorPasser(dataObj, filename, newDateString, ebiddingDocType, errorNo);
                    listEbiddingError.add(parseError);

                    totalRecordError++;
              }
            String statusType = totalRecordError > 0?"Error":"Pass";
            mapLog.put("filename",filename);
            mapLog.put("transdate",transDate);
            mapLog.put("doctype",ebiddingDocType);
            mapLog.put("statusType",statusType);
            resultListLog.add(mapLog);

            //Delete File Temp After Process
            try {
                Files.delete(Paths.get(fileProcess));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        resultObject.add(resultListLog);
        for (EbiddingErrorDto errorData : listEbiddingError) {
            Map<String, Object> map = new HashMap<>();
            map.put("filename", errorData.getFilename());
            map.put("transdate", errorData.getTransdate());
            map.put("doctype", errorData.getDoctype());
            map.put("itemno", errorData.getItemno());
            map.put("message", errorData.getMessage());
            resultList.add(map);
        }
        resultObject.add(resultList);
        listEbiddingError.clear();
        fileStorageService.MoveFiletoSuccessDirDate(filename);
        return resultObject;
    }
    public int jobRunnerPostFile(String filename) {
        int statusInsert = 0;
        List<String> fileInOutput = scanFilePath.listFilesOutputForFolder(new File(fileStorageService.getFileOutputLocation().toString()), filename);
        if(fileInOutput.size() >= 1){
            fileStorageService.deleteFileSuccess(filename);
        }
        List<String> fileTempList = fileStorageService.extractFileLine(filename);
        int countList = fileTempList.size();
        if (countList == 0) {
            textEbiddingCheck.validateFileNull(filename);
        }
        int countLog = 0;
        for (String fileProcess : fileTempList) {
            countList--;
            List<String> fileByline = fileStorageService.readFilebyLine(fileProcess);
            EbiddingHeaderItemDto headerLine = textEbiddingValidate.splitLineHeader(fileByline.get(fileByline.size() - fileByline.size()));
            EbiddingFooterItemDto endLine = textEbiddingValidate.splitLineFooter(fileByline.get(fileByline.size() - 1));

            // insert start process log to database
            File fileName = new File(fileProcess);
            String getFileName = fileName.getName();
            String ebiddingDocType = getFileName.substring(9, 11);
            String dateCode = filename.substring(13, 21);
            String newDateString = textEbiddingValidate.dateFormat(dateCode);
            int countLogSame = ebiddingLogDao.countLogSame(filename, newDateString, ebiddingDocType);
            if (countLogSame == 0) {
                EbiddingLogDto saveLog = new EbiddingLogDto();
                saveLog.setFilename(filename);
                saveLog.setTransdate(newDateString);
                saveLog.setDoctype(ebiddingDocType);
                saveLog.setStatus("C");
                saveLog.setTotamt(0);
                saveLog.setTotrec(0);
                saveLog.setPostrec(0);
                saveLog.setErrrec(0);
                String formatDateSaveLog = new SimpleDateFormat("dd-MMM-yy HH:mm:ss").format(new Date());
                final String dateTime = formatDateSaveLog;
                saveLog.setUploadstart(dateTime);
                try {
                    ebiddingLogDao.addLog(saveLog);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                // check process error to database
                double totalAmount = (double) 0;
                Integer countTotalRecord = 0;
                Integer totalRecordPost = 0;
                Integer totalRecordError = 0;
                boolean haveErrorRecordPost = false;
                List<EbiddingErrorDto> listEbiddingError = new ArrayList<>();

                // check error Excel Number 01
                int countTransDate = ebiddingItemDao.countTransDate(filename, newDateString, ebiddingDocType);
                if (countTransDate >= 1) {
                    EbiddingItemDto dataObj = new EbiddingItemDto();
                    dataObj.setItemno(0);
                    Integer errorNo = 1;

                    EbiddingErrorDto parseError = ErrorInsertService.errorPasser(dataObj, filename, newDateString, ebiddingDocType, errorNo);
                    listEbiddingError.add(parseError);

                    totalRecordError++;
                    haveErrorRecordPost = true;
                }
                // check error Excel Number 03
                if ((fileByline.size() - 2) <= 0) {
                    EbiddingItemDto dataObj = new EbiddingItemDto();
                    dataObj.setItemno(0);
                    Integer errorNo = 3;

                    EbiddingErrorDto parseError = ErrorInsertService.errorPasser(dataObj, filename, newDateString, ebiddingDocType, errorNo);
                    listEbiddingError.add(parseError);

                    totalRecordError++;
                    haveErrorRecordPost = true;
                }
                EbiddingFooterItemDto endLinedata = textEbiddingValidate.splitLineFooter(fileByline.get(fileByline.size() - 1));
                List<EbiddingItemDto> listEbiddingItemCheckPass = new ArrayList<>();
                List<CompletableFuture<EbiddingCheckDto>> checkResultAllList = new ArrayList<>();
                int row = 0;
                for (String line : fileByline) {
                    boolean haveErrorCheck = false;
                    row = row + 1;
                    // skip first line and end line
                    if (row == 1 || row == fileByline.size()) {
                        continue;
                    }
                    CompletableFuture<EbiddingCheckDto> resultFutureError = checkErrorService.checkProcess(line, ebiddingDocType, row, headerLine, endLine);
                    checkResultAllList.add(resultFutureError);
                    countTotalRecord++;
                }
                //End For Text line
                for (Future<EbiddingCheckDto> futureError : checkResultAllList) {
                    try {
                        EbiddingCheckDto resultEbiddingCheck = futureError.get();

                        boolean haveError = resultEbiddingCheck.isHaveError();
                        EbiddingItemDto ebiddingItemDto = resultEbiddingCheck.getEbiddingItemDto();
                        totalAmount = totalAmount + convertStringToDouble(ebiddingItemDto.getAmount());

                        if (haveError) {
                            List<Integer> errorNumberList = resultEbiddingCheck.getErrorList();
                            int numberOfError = errorNumberList.size();
                            totalRecordError = totalRecordError + numberOfError;
                            haveErrorRecordPost = true;

                            for (int errorNumber : errorNumberList) {
                                EbiddingErrorDto parseError = ErrorInsertService.errorPasser(ebiddingItemDto, filename, newDateString, ebiddingDocType, errorNumber);
                                listEbiddingError.add(parseError);
                            }

                        } else {
                            listEbiddingItemCheckPass.add(ebiddingItemDto);
                            totalRecordPost++;
                        }
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                    }
                }
                checkResultAllList.clear();
                fileByline.clear();
                // check error Excel Number 05
                if (!textEbiddingValidate.validateTotalAmount(totalAmount, endLinedata)) {
                    EbiddingItemDto dataObj = new EbiddingItemDto();
                    dataObj.setItemno(9999999);
                    Integer errorNo = 5;

                    EbiddingErrorDto parseError = ErrorInsertService.errorPasser(dataObj, filename, newDateString, ebiddingDocType, errorNo);
                    listEbiddingError.add(parseError);

                    totalRecordError++;
                    haveErrorRecordPost = true;
                }
                //check error Excel Number 12
                int sumAmountStatement = ebiddingStatementDao.sumAmount(newDateString,dateCode,ebiddingDocType);
                Integer objAmountSTM = new Integer(sumAmountStatement);
                int valueTotalAmount1 = (int) totalAmount;
                Integer objAmountEBD = new Integer(valueTotalAmount1);

                if (!objAmountSTM.equals(objAmountEBD)) {
                    EbiddingItemDto dataObj = new EbiddingItemDto();
                    dataObj.setItemno(0);
                    Integer errorNo = 12;

                    EbiddingErrorDto parseError = ErrorInsertService.errorPasser(dataObj, filename, newDateString, ebiddingDocType, errorNo);
                    listEbiddingError.add(parseError);

                    totalRecordError++;
                    haveErrorRecordPost = true;
                }
                /*
                    Start Insert Data to Transinterface if no error
                */
                if (totalRecordError == 0) {
                    final int batchSize = 500;
                    int count = 0;

                    List<EbiddingItemDto> batchEbiddingItemList = new ArrayList<EbiddingItemDto>();
                    ArrayList<EbiddingItemDto[]> processEbiddingItemList = new ArrayList<EbiddingItemDto[]>();
                    List<CompletableFuture<int[][]>> resultFutureAllInsert = new ArrayList<>();

                    for (EbiddingItemDto insertDataObj : listEbiddingItemCheckPass) {

                        insertDataObj.setFilename(filename);
                        insertDataObj.setDoctype(ebiddingDocType);
                        insertDataObj.setTransdate(newDateString);
                        batchEbiddingItemList.add(insertDataObj);

                        if (count % batchSize == 0 || (((listEbiddingItemCheckPass.size() - 1) % batchSize) != 0 && (listEbiddingItemCheckPass.size() - 1) == count)) {

                            EbiddingItemDto[] addbatchEbiddingItemList = batchEbiddingItemList.toArray(new EbiddingItemDto[batchEbiddingItemList.size()]);
                            processEbiddingItemList.add(addbatchEbiddingItemList);

                            batchEbiddingItemList.clear();
                        }
                        count++;
                    }

                    for (EbiddingItemDto[] batchlist : processEbiddingItemList) {

                        Collection<EbiddingItemDto> list = Arrays.asList(batchlist);
                        try {
                            CompletableFuture<int[][]> resultInsert = ebiddingItemDao.batchAdd(list, list.size());
                            resultFutureAllInsert.add(resultInsert);
                            //resultapp.join();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    for (Future<int[][]> futureInsert : resultFutureAllInsert) {
                        try {
                            futureInsert.get();
                        } catch (InterruptedException | ExecutionException e) {
                            e.printStackTrace();
                        }
                    }
                    resultFutureAllInsert.clear();

                }
                 /*
                    Start Insert Data to TransError if have error
                */
                if (totalRecordError > 0) {

                    final int batchSize = 500;
                    int count = 0;

                    List<EbiddingErrorDto> batchEbiddingErrorList = new ArrayList<EbiddingErrorDto>();

                    ArrayList<EbiddingErrorDto[]> processTransErrorList = new ArrayList<EbiddingErrorDto[]>();

                    List<CompletableFuture<int[][]>> resultFutureAddError = new ArrayList<>();

                    fileStorageService.fileOutPass(filename, dateCode, ebiddingDocType, "Error");

                    for (EbiddingErrorDto insertErrorData : listEbiddingError) {
                        fileStorageService.textFileError(insertErrorData, dateCode);
                        batchEbiddingErrorList.add(insertErrorData);

                        if (count % batchSize == 0 || (((listEbiddingError.size() - 1) % batchSize) != 0 && (listEbiddingError.size() - 1) == count)) {
                            EbiddingErrorDto[] addBatchEbiddingErrorList = batchEbiddingErrorList.toArray(new EbiddingErrorDto[batchEbiddingErrorList.size()]);
                            processTransErrorList.add(addBatchEbiddingErrorList);
                            batchEbiddingErrorList.clear();
                        }
                        count++;
                    }

                    for (EbiddingErrorDto[] batchlist : processTransErrorList) {
                        Collection<EbiddingErrorDto> list = Arrays.asList(batchlist);
                        try {
                            CompletableFuture<int[][]> resultAddError = ebiddingErrorDao.batchAdd(list, list.size());
                            resultFutureAddError.add(resultAddError);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    for (Future<int[][]> futureError : resultFutureAddError) {
                        try {
                            futureError.get();
                        } catch (InterruptedException | ExecutionException e) {
                            e.printStackTrace();
                        }
                    }
                    resultFutureAddError.clear();
                }
                if (haveErrorRecordPost == true) {
                    totalRecordPost = 0;
                }
                EbiddingLogDto updateLog = new EbiddingLogDto();
                if (totalRecordError.intValue() == 0) {
                    String dateCodeUpdate = filename.substring(13, 21);
                    String dateStringUpdate = textEbiddingValidate.dateFormat(dateCodeUpdate);
                    updateLog.setTransdate(dateStringUpdate);
                    updateLog.setDoctype(ebiddingDocType);
                    updateLog.setFilename(filename);
                    updateLog.setTotamt(totalAmount);
                    updateLog.setTotrec(countTotalRecord);
                    updateLog.setPostrec(totalRecordPost);
                    updateLog.setErrrec(totalRecordError);
                    updateLog.setStatus("A");
                    String formatDateUpdateLog = new SimpleDateFormat("dd-MMM-yy HH:mm:ss").format(new Date());
                    updateLog.setUploadfinish(formatDateUpdateLog);
                    try {
                        fileStorageService.fileOutPass(filename, dateCode, ebiddingDocType, "Pass");
                        ebiddingLogDao.updateLogCode(updateLog, dateTime);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    String dateCodeUpdate = filename.substring(13, 21);
                    String dateStringUpdate = textEbiddingValidate.dateFormat(dateCodeUpdate);
                    updateLog.setTransdate(dateStringUpdate);
                    updateLog.setDoctype(ebiddingDocType);
                    updateLog.setFilename(filename);
                    updateLog.setTotamt(totalAmount);
                    updateLog.setTotrec(countTotalRecord);
                    updateLog.setPostrec(totalRecordPost);
                    updateLog.setErrrec(totalRecordError);
                    updateLog.setStatus("F");
                    String formatDateUpdateLog = new SimpleDateFormat("dd-MMM-yy HH:mm:ss").format(new Date());
                    updateLog.setUploadfinish(formatDateUpdateLog);
                    try {
                        ebiddingLogDao.updateLogCode(updateLog, dateTime);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                countLog = countLog + 1;
                if (countLog == countList) {
                    fileStorageService.deleteFileToProcessDir(filename);
                }
            }
            //Delete File Temp After Process
            try {
                Files.delete(Paths.get(fileProcess));
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (countList == 0) {
                int countLogError = ebiddingErrorDao.countError(filename);
                if (countLogError > 0) {
                    try {
                        ebiddingErrorDao.deleteItem(filename);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    fileStorageService.MoveFileToErrorDirDate(filename);
                    try {
                        ebiddingLogDao.updateLogStatusError(filename, newDateString);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    statusInsert = 3;
                } else {
                    fileStorageService.MoveFiletoSuccessDirDate(filename);
                    try {
                        ebiddingLogDao.updateLogStatusPost(filename, newDateString);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    statusInsert = 1;
                }
            }
        }
        return statusInsert;
    }
    public List<Map<String, Object>> jobRunnerTestStatement(String projectFile, String transDate, String fileDate,int countFile) {
        Integer totalRecordError = 0;
        List<Map<String, Object>> resultList = new ArrayList<>();
        Map<String, Object> fileName = new HashMap<>();
        fileName.put("fileName", projectFile);
        resultList.add(fileName);
        //1.ไม่พบไฟล์ที่นำเข้าตามรูปแบบที่กำหนด
        if (countFile <= 0) {
            Map<String, Object> map = new HashMap<>();
            map.put("errorId", 1);
            resultList.add(map);
            totalRecordError++;
        } else {
            Map<String, Object> map = new HashMap<>();
            map.put("errorId", 0);
            resultList.add(map);
        }
        //2.รายการได้ถูกบันทึกแล้ว
        int StatusInsertStatement = ebiddingStatementDao.StatusInsertStatement(projectFile);
        if (StatusInsertStatement > 0) {
            Map<String, Object> map = new HashMap<>();
            map.put("errorId", 2);
            resultList.add(map);
            totalRecordError++;
        } else {
            Map<String, Object> map = new HashMap<>();
            map.put("errorId", 0);
            resultList.add(map);
        }
        //3.วันที่ผ่านรายการแตกต่างกับวันที่ของชื่อไฟล์
        if (textEbiddingValidate.checkFileNameStatement(projectFile, fileDate)) {
            Map<String, Object> map = new HashMap<>();
            map.put("errorId", 3);
            resultList.add(map);
            totalRecordError++;
        } else {
            Map<String, Object> map = new HashMap<>();
            map.put("errorId", 0);
            resultList.add(map);
        }

        fileStorageService.moveFileToProcessDir(projectFile);
        String uploadDate = new SimpleDateFormat("dd-MMM-yy HH:mm:ss").format(new Date());
        String projectCode = projectFile;

        List<String> scanFileName = scanFilePath.listConvertAndPostFilesForFolder(new File(fileStorageService.getFileStorageLocation().toString()), projectCode);
        //4.จำนวนเงินรวมไม่เท่ากับจำนวนเงินของบรรทัด
        if (scanFileName.size() <= 0) {
            Map<String, Object> map = new HashMap<>();
            map.put("errorId", 0);
            resultList.add(map);
        }
        for (String filename : scanFileName) {
            File file = new File(fileStorageService.getFileStorageLocation().toString() + "/" + filename);
            String filenameFullPath = file.getAbsolutePath();
            List<String> fileByline = fileStorageService.readFilebyLine(filenameFullPath);

            int row = 0;
            for (String line : fileByline) {
                row = row + 1;
                if (row == 1) {
                    continue;
                }
                EbiddingStatementDto insertDataObj = textEbiddingValidate.splitlinetoObjStatement(line);
                //4.จำนวนเงินรวมไม่เท่ากับจำนวนเงินของบรรทัด
                if (!textEbiddingValidate.checkFileNameStatementNetAmount(insertDataObj)) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("errorId", 4);
                    resultList.add(map);
                    totalRecordError++;
                } else {
                    Map<String, Object> map = new HashMap<>();
                    map.put("errorId", 0);
                    resultList.add(map);
                }

                String dateForFile = textEbiddingValidate.dateFormatFileText(transDate);
//                    int countInsert = ebiddingStatementDao.countInsert(filename);
                try {
                    if (totalRecordError.equals(0)) {
                        ebiddingStatementDao.testProcess(insertDataObj, dateForFile, uploadDate, filename,"T");
//                            StatusInsert = ebiddingStatementDao.update(insertDataObj, uploadDate, filename);
                    }
//                        else {
//                            StatusInsert = ebiddingStatementDao.add(insertDataObj, dateForFile, uploadDate, filename);
//                        }
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                try {
//                    fileStorageService.MoveFileStatementSuccessDirDate(filename);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
            }
        }
        //5.ไม่พบไฟล์ข้อมูลตามเงื่อนไขที่ต้องการ
        if (countFile <= 0) {
            Map<String, Object> map = new HashMap<>();
            map.put("errorId", 5);
            resultList.add(map);
            totalRecordError++;
        } else {
            Map<String, Object> map = new HashMap<>();
            map.put("errorId", 0);
            resultList.add(map);
        }
        String status = totalRecordError.equals(0)?"PASS":"ERROR";
        Map<String, Object> statusFile = new HashMap<>();
        statusFile.put("statusFile", status);
        resultList.add(statusFile);
        if (countFile > 0) {
            try {
                fileStorageService.MoveFileStatementSuccessDirDate(projectFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return resultList;
    }
    public List<Map<String, Object>> jobRunnerProcessStatement(String projectFile, String transDate, String fileDate,int countFile) {
        Integer totalRecordError = 0;
        List<Map<String, Object>> resultList = new ArrayList<>();
        Map<String, Object> fileName = new HashMap<>();
        fileName.put("fileName", projectFile);
        resultList.add(fileName);
        //1.ไม่พบไฟล์ที่นำเข้าตามรูปแบบที่กำหนด
        if (countFile <= 0) {
            Map<String, Object> map = new HashMap<>();
            map.put("errorId", 1);
            resultList.add(map);
            totalRecordError++;
        } else {
            Map<String, Object> map = new HashMap<>();
            map.put("errorId", 0);
            resultList.add(map);
        }
        //2.รายการได้ถูกบันทึกแล้ว
        int StatusInsertStatement = ebiddingStatementDao.StatusInsertStatement(projectFile);
        if (StatusInsertStatement > 0) {
            Map<String, Object> map = new HashMap<>();
            map.put("errorId", 2);
            resultList.add(map);
            totalRecordError++;
        } else {
            Map<String, Object> map = new HashMap<>();
            map.put("errorId", 0);
            resultList.add(map);
        }
        //3.วันที่ผ่านรายการแตกต่างกับวันที่ของชื่อไฟล์
        if (textEbiddingValidate.checkFileNameStatement(projectFile, fileDate)) {
            Map<String, Object> map = new HashMap<>();
            map.put("errorId", 3);
            resultList.add(map);
            totalRecordError++;
        } else {
            Map<String, Object> map = new HashMap<>();
            map.put("errorId", 0);
            resultList.add(map);
        }

        fileStorageService.moveFileToProcessDir(projectFile);
        String uploadDate = new SimpleDateFormat("dd-MMM-yy HH:mm:ss").format(new Date());
        String projectCode = projectFile;

        List<String> scanFileName = scanFilePath.listConvertAndPostFilesForFolder(new File(fileStorageService.getFileStorageLocation().toString()), projectCode);
        //4.จำนวนเงินรวมไม่เท่ากับจำนวนเงินของบรรทัด
        if (scanFileName.size() <= 0) {
            Map<String, Object> map = new HashMap<>();
            map.put("errorId", 0);
            resultList.add(map);
        }
        for (String filename : scanFileName) {
            File file = new File(fileStorageService.getFileStorageLocation().toString() + "/" + filename);
            String filenameFullPath = file.getAbsolutePath();
            List<String> fileByline = fileStorageService.readFilebyLine(filenameFullPath);

            int row = 0;
            for (String line : fileByline) {
                row = row + 1;
                if (row == 1) {
                    continue;
                }
                EbiddingStatementDto insertDataObj = textEbiddingValidate.splitlinetoObjStatement(line);
                //4.จำนวนเงินรวมไม่เท่ากับจำนวนเงินของบรรทัด
                if (!textEbiddingValidate.checkFileNameStatementNetAmount(insertDataObj)) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("errorId", 4);
                    resultList.add(map);
                    totalRecordError++;
                } else {
                    Map<String, Object> map = new HashMap<>();
                    map.put("errorId", 0);
                    resultList.add(map);
                }

                String dateForFile = textEbiddingValidate.dateFormatFileText(transDate);
//                    int countInsert = ebiddingStatementDao.countInsert(filename);
                try {
                    if (totalRecordError.equals(0)) {
                        ebiddingStatementDao.testProcess(insertDataObj, dateForFile, uploadDate, filename,"P");
//                            StatusInsert = ebiddingStatementDao.update(insertDataObj, uploadDate, filename);
                    }
//                        else {
//                            StatusInsert = ebiddingStatementDao.add(insertDataObj, dateForFile, uploadDate, filename);
//                        }
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                try {
//                    fileStorageService.MoveFileStatementSuccessDirDate(filename);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
            }
        }
        //5.ไม่พบไฟล์ข้อมูลตามเงื่อนไขที่ต้องการ
        if (countFile <= 0) {
            Map<String, Object> map = new HashMap<>();
            map.put("errorId", 5);
            resultList.add(map);
            totalRecordError++;
        } else {
            Map<String, Object> map = new HashMap<>();
            map.put("errorId", 0);
            resultList.add(map);
        }
        String status = totalRecordError.equals(0)?"PASS":"ERROR";
        Map<String, Object> statusFile = new HashMap<>();
        statusFile.put("statusFile", status);
        resultList.add(statusFile);
        if (countFile > 0) {
            try {
                fileStorageService.MoveFileStatementSuccessDirDate(projectFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return resultList;
    }
//    public int jobRunnerPostStatement(String fileName,String transDate) {
//        String uploadDate = new SimpleDateFormat("dd-MMM-yy HH:mm:ss").format(new Date());
//        String projectCode = fileName;
//        int StatusInsert = 0;
//        List<String> scanFileName = scanFilePath.listConvertAndPostFilesForFolder(new File(fileStorageService.getFileStorageLocation().toString()), projectCode);
//
//        for (String filename : scanFileName) {
//            File file = new File(fileStorageService.getFileStorageLocation().toString() + "/" + filename);
//            String filenameFullPath = file.getAbsolutePath();
//            List<String> fileByline = fileStorageService.readFilebyLine(filenameFullPath);
//            ArrayList<Object> saveError = new ArrayList<>();
//            int row = 0;
//            for (String line : fileByline) {
//                row = row + 1;
//                if (row == 1) {
//                    continue;
//                }
//                EbiddingStatementDto insertDataObj = textEbiddingValidate.splitlinetoObjStatement(line);
//
//                String dateForFile = textEbiddingValidate.dateFormatFileText(transDate);
//                int countInsert = ebiddingStatementDao.countInsert(filename);
//                try {
//                    if (countInsert > 0) {
//                        StatusInsert = ebiddingStatementDao.update(insertDataObj, uploadDate, filename);
//                    } else {
//                        StatusInsert = ebiddingStatementDao.add(insertDataObj, dateForFile, uploadDate, filename);
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                try {
//                    fileStorageService.MoveFileStatementSuccessDirDate(filename);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        return StatusInsert;
//    }
    private double convertStringToDouble(String number) {
        double money;
        try {
            return Double.parseDouble(number);
        } catch (NumberFormatException ex) {
            return (double) 0;
        } catch (NullPointerException ex) {
            return (double) 0;
        }
    }
}
