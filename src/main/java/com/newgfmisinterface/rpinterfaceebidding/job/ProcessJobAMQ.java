package com.newgfmisinterface.rpinterfaceebidding.job;

import java.io.File;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.newgfmisinterface.rpinterfaceebidding.dao.EbiddingActiveMQDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.newgfmisinterface.rpinterfaceebidding.dto.MqMsgDto;
import com.newgfmisinterface.rpinterfaceebidding.dto.XmlData;
//import com.newgfmisinterface.rpinterfaceebidding.queue.SenderQueue;
import com.newgfmisinterface.rpinterfaceebidding.utils.AES;

@Component
public class ProcessJobAMQ {
    private static Logger LOGGER = LoggerFactory.getLogger(ProcessJobAMQ.class);

    @Autowired
    private EbiddingActiveMQDao ebiddingActiveMQDao;

    public void jobRunner(String filename, int checkLoop) {
        String docTypeNo1 = filename.substring(9, 11);
        String docTypeNo2 = filename.substring(11, 13);
        String[] docType = {docTypeNo1, docTypeNo2};
        for (String fileDocType : docType) {
//            String startDate = new SimpleDateFormat("dd-MMM-yy HH:mm:ss").format(new Date());
            try {
                int countPostStart = ebiddingActiveMQDao.countPostStart(filename,fileDocType);
                if(countPostStart == 1){
                    String startDate = new SimpleDateFormat("dd-MMM-yy HH:mm:ss").format(new Date());
                    ebiddingActiveMQDao.updateLogActiveMQStartPost(filename,fileDocType,startDate);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            List<Map<String, Object>> resultList = null;
            try {
                if (checkLoop == 1) {
                    resultList = ebiddingActiveMQDao.getEbiddingList(filename,fileDocType);
                } else {
                    resultList = ebiddingActiveMQDao.getEbiddingListMultipleLoop(filename,fileDocType);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            int sizeItem = resultList == null ? 0 : resultList.size();
            if (sizeItem >= 1) {
                for (int i = 0; i < resultList.size(); i++) {
                    XmlData saveXmlData = new XmlData();
                    saveXmlData.setFilename((String) resultList.get(i).get("FILE_NAME"));
                    saveXmlData.setTransdate((String) resultList.get(i).get("TRANS_DATE"));
                    saveXmlData.setDoctype((String) resultList.get(i).get("DOC_TYPE"));
                    saveXmlData.setItemno(resultList.get(i).get("ITEMNO") + "");
                    saveXmlData.setCct((String) resultList.get(i).get("CCT"));
                    saveXmlData.setCctowner((String) resultList.get(i).get("CCT_OWNER"));
                    saveXmlData.setXref3((String) resultList.get(i).get("XREF3"));
                    saveXmlData.setZzdeposit((String) resultList.get(i).get("ZZDEPOSIT"));
                    saveXmlData.setZzowner((String) resultList.get(i).get("ZZOWNER"));
                    saveXmlData.setAmount(resultList.get(i).get("AMOUNT") + "");
                    try {
                        int countLoop = ebiddingActiveMQDao.countLoop(saveXmlData);
                        ebiddingActiveMQDao.getActiveQueue(saveXmlData, countLoop);
                    } catch (JAXBException e) {
                        e.printStackTrace();
                    }
                }
            }
            try {
                ebiddingActiveMQDao.updateLogActiveMQEndPost(filename,fileDocType);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
//    public void jobCheckActiveMQ(String filename) {
//
//    }
}
