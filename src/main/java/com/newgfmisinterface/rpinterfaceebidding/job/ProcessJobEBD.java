package com.newgfmisinterface.rpinterfaceebidding.job;

import com.newgfmisinterface.rpinterfaceebidding.dao.EbiddingErrorDao;
import com.newgfmisinterface.rpinterfaceebidding.dao.EbiddingItemDao;
import com.newgfmisinterface.rpinterfaceebidding.dao.EbiddingLogDao;
import com.newgfmisinterface.rpinterfaceebidding.dao.EbiddingStatementDao;
import com.newgfmisinterface.rpinterfaceebidding.dto.EbiddingItemDto;
import com.newgfmisinterface.rpinterfaceebidding.dto.EbiddingFooterItemDto;
import com.newgfmisinterface.rpinterfaceebidding.dto.EbiddingHeaderItemDto;
import com.newgfmisinterface.rpinterfaceebidding.dto.EbiddingLogDto;
import com.newgfmisinterface.rpinterfaceebidding.dto.EbiddingErrorDto;
import com.newgfmisinterface.rpinterfaceebidding.dto.EbiddingCheckDto;
import com.newgfmisinterface.rpinterfaceebidding.service.CheckErrorService;
import com.newgfmisinterface.rpinterfaceebidding.service.ErrorInsertService;
import com.newgfmisinterface.rpinterfaceebidding.service.FileStorageService;
import com.newgfmisinterface.rpinterfaceebidding.service.ScanFilePathService;
import com.newgfmisinterface.rpinterfaceebidding.validate.TextEbiddingCheck;
import com.newgfmisinterface.rpinterfaceebidding.validate.TextEbiddingValidate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Component
public class ProcessJobEBD {
    private static Logger LOGGER = LoggerFactory.getLogger(ProcessJobEBD.class);

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private ScanFilePathService scanFilePath;

    @Autowired
    private EbiddingLogDao ebiddingLogDao;

    @Autowired
    private EbiddingErrorDao ebiddingErrorDao;

    @Autowired
    private EbiddingItemDao ebiddingItemDao;

    @Autowired
    private EbiddingStatementDao ebiddingStatementDao;

    @Autowired
    private TextEbiddingCheck textEbiddingCheck;

    @Autowired
    private CheckErrorService checkErrorService;

    private TextEbiddingValidate textEbiddingValidate = new TextEbiddingValidate();

    public void jobRunner(String filename) {
        List<String> fileInOutput = scanFilePath.listFilesOutputForFolder(new File(fileStorageService.getFileOutputLocation().toString()), filename);
        if(fileInOutput.size() >= 1){
            fileStorageService.deleteFileSuccess(filename);
        }
        List<String> fileTempList = fileStorageService.extractFileLine(filename);
        int countList = fileTempList.size();
        if (countList == 0) {
            textEbiddingCheck.validateFileNull(filename);
        }
        int countLog = 0;
        for (String fileProcess : fileTempList) {
            countList--;
            List<String> fileByline = fileStorageService.readFilebyLine(fileProcess);
            EbiddingHeaderItemDto headerLine = textEbiddingValidate.splitLineHeader(fileByline.get(fileByline.size() - fileByline.size()));
            EbiddingFooterItemDto endLine = textEbiddingValidate.splitLineFooter(fileByline.get(fileByline.size() - 1));

            // insert start process log to database
            File fileName = new File(fileProcess);
            String getFileName = fileName.getName();
            String ebiddingDocType = getFileName.substring(9, 11);
            String dateCode = filename.substring(13, 21);
            String newDateString = textEbiddingValidate.dateFormat(dateCode);
            int countLogSame = ebiddingLogDao.countLogSame(filename, newDateString, ebiddingDocType);
            if (countLogSame == 0) {
                EbiddingLogDto saveLog = new EbiddingLogDto();
                saveLog.setFilename(filename);
                saveLog.setTransdate(newDateString);
                saveLog.setDoctype(ebiddingDocType);
                saveLog.setStatus("C");
                saveLog.setTotamt(0);
                saveLog.setTotrec(0);
                saveLog.setPostrec(0);
                saveLog.setErrrec(0);
                String formatDateSaveLog = new SimpleDateFormat("dd-MMM-yy HH:mm:ss").format(new Date());
                final String dateTime = formatDateSaveLog;
                saveLog.setUploadstart(dateTime);
                try {
                    ebiddingLogDao.addLog(saveLog);
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                // check process error to database
                double totalAmount = (double) 0;
                Integer countTotalRecord = 0;
                Integer totalRecordPost = 0;
                Integer totalRecordError = 0;
                boolean haveErrorRecordPost = false;
                List<EbiddingErrorDto> listEbiddingError = new ArrayList<>();

                // check error Excel Number 01
                int countTransDate = ebiddingItemDao.countTransDate(filename, newDateString, ebiddingDocType);
                if (countTransDate >= 1) {
                    EbiddingItemDto dataObj = new EbiddingItemDto();
                    dataObj.setItemno(0);
                    Integer errorNo = 1;

                    EbiddingErrorDto parseError = ErrorInsertService.errorPasser(dataObj, filename, newDateString, ebiddingDocType, errorNo);
                    listEbiddingError.add(parseError);

                    totalRecordError++;
                    haveErrorRecordPost = true;
                }
                // check error Excel Number 03
                if ((fileByline.size() - 2) <= 0) {
                    EbiddingItemDto dataObj = new EbiddingItemDto();
                    dataObj.setItemno(0);
                    Integer errorNo = 3;

                    EbiddingErrorDto parseError = ErrorInsertService.errorPasser(dataObj, filename, newDateString, ebiddingDocType, errorNo);
                    listEbiddingError.add(parseError);

                    totalRecordError++;
                    haveErrorRecordPost = true;
                }
                EbiddingFooterItemDto endLinedata = textEbiddingValidate.splitLineFooter(fileByline.get(fileByline.size() - 1));
                List<EbiddingItemDto> listEbiddingItemCheckPass = new ArrayList<>();
                List<CompletableFuture<EbiddingCheckDto>> checkResultAllList = new ArrayList<>();
                int row = 0;
                for (String line : fileByline) {
                    boolean haveErrorCheck = false;
                    row = row + 1;
                    // skip first line and end line
                    if (row == 1 || row == fileByline.size()) {
                        continue;
                    }
                    CompletableFuture<EbiddingCheckDto> resultFutureError = checkErrorService.checkProcess(line, ebiddingDocType, row, headerLine, endLine);
                    checkResultAllList.add(resultFutureError);
                    countTotalRecord++;
                }
                //End For Text line
                for (Future<EbiddingCheckDto> futureError : checkResultAllList) {
                    try {
                        EbiddingCheckDto resultEbiddingCheck = futureError.get();

                        boolean haveError = resultEbiddingCheck.isHaveError();
                        EbiddingItemDto ebiddingItemDto = resultEbiddingCheck.getEbiddingItemDto();
                        totalAmount = totalAmount + convertStringToDouble(ebiddingItemDto.getAmount());

                        if (haveError) {
                            List<Integer> errorNumberList = resultEbiddingCheck.getErrorList();
                            int numberOfError = errorNumberList.size();
                            totalRecordError = totalRecordError + numberOfError;
                            haveErrorRecordPost = true;

                            for (int errorNumber : errorNumberList) {
                                EbiddingErrorDto parseError = ErrorInsertService.errorPasser(ebiddingItemDto, filename, newDateString, ebiddingDocType, errorNumber);
                                listEbiddingError.add(parseError);
                            }

                        } else {
                            listEbiddingItemCheckPass.add(ebiddingItemDto);
                            totalRecordPost++;
                        }
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                    }
                }
                checkResultAllList.clear();
                fileByline.clear();
                // check error Excel Number 05
                if (!textEbiddingValidate.validateTotalAmount(totalAmount, endLinedata)) {
                    EbiddingItemDto dataObj = new EbiddingItemDto();
                    dataObj.setItemno(9999999);
                    Integer errorNo = 5;

                    EbiddingErrorDto parseError = ErrorInsertService.errorPasser(dataObj, filename, newDateString, ebiddingDocType, errorNo);
                    listEbiddingError.add(parseError);

                    totalRecordError++;
                    haveErrorRecordPost = true;
                }
                //check error Excel Number 12
                int sumAmountStatement = ebiddingStatementDao.sumAmount(newDateString,dateCode,ebiddingDocType);
                Integer objAmountSTM = new Integer(sumAmountStatement);
                int valueTotalAmount1 = (int) totalAmount;
                Integer objAmountEBD = new Integer(valueTotalAmount1);

                if (!objAmountSTM.equals(objAmountEBD)) {
                    EbiddingItemDto dataObj = new EbiddingItemDto();
                    dataObj.setItemno(0);
                    Integer errorNo = 12;

                    EbiddingErrorDto parseError = ErrorInsertService.errorPasser(dataObj, filename, newDateString, ebiddingDocType, errorNo);
                    listEbiddingError.add(parseError);

                    totalRecordError++;
                    haveErrorRecordPost = true;
                }
                /*
                    Start Insert Data to Transinterface if no error
                */
                if (totalRecordError == 0) {
                    final int batchSize = 500;
                    int count = 0;

                    List<EbiddingItemDto> batchEbiddingItemList = new ArrayList<EbiddingItemDto>();
                    ArrayList<EbiddingItemDto[]> processEbiddingItemList = new ArrayList<EbiddingItemDto[]>();
                    List<CompletableFuture<int[][]>> resultFutureAllInsert = new ArrayList<>();

                    for (EbiddingItemDto insertDataObj : listEbiddingItemCheckPass) {

                        insertDataObj.setFilename(filename);
                        insertDataObj.setDoctype(ebiddingDocType);
                        insertDataObj.setTransdate(newDateString);
                        batchEbiddingItemList.add(insertDataObj);

                        if (count % batchSize == 0 || (((listEbiddingItemCheckPass.size() - 1) % batchSize) != 0 && (listEbiddingItemCheckPass.size() - 1) == count)) {

                            EbiddingItemDto[] addbatchEbiddingItemList = batchEbiddingItemList.toArray(new EbiddingItemDto[batchEbiddingItemList.size()]);
                            processEbiddingItemList.add(addbatchEbiddingItemList);

                            batchEbiddingItemList.clear();
                        }
                        count++;
                    }

                    for (EbiddingItemDto[] batchlist : processEbiddingItemList) {

                        Collection<EbiddingItemDto> list = Arrays.asList(batchlist);
                        try {
                            CompletableFuture<int[][]> resultInsert = ebiddingItemDao.batchAdd(list, list.size());
                            resultFutureAllInsert.add(resultInsert);
                            //resultapp.join();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    for (Future<int[][]> futureInsert : resultFutureAllInsert) {
                        try {
                            futureInsert.get();
                        } catch (InterruptedException | ExecutionException e) {
                            e.printStackTrace();
                        }
                    }
                    resultFutureAllInsert.clear();

                }
                 /*
                    Start Insert Data to TransError if have error
                */
                if (totalRecordError > 0) {

                    final int batchSize = 500;
                    int count = 0;

                    List<EbiddingErrorDto> batchEbiddingErrorList = new ArrayList<EbiddingErrorDto>();

                    ArrayList<EbiddingErrorDto[]> processTransErrorList = new ArrayList<EbiddingErrorDto[]>();

                    List<CompletableFuture<int[][]>> resultFutureAddError = new ArrayList<>();

                    fileStorageService.fileOutPass(filename, dateCode, ebiddingDocType, "Error");

                    for (EbiddingErrorDto insertErrorData : listEbiddingError) {
                        fileStorageService.textFileError(insertErrorData, dateCode);
                        batchEbiddingErrorList.add(insertErrorData);

                        if (count % batchSize == 0 || (((listEbiddingError.size() - 1) % batchSize) != 0 && (listEbiddingError.size() - 1) == count)) {
                            EbiddingErrorDto[] addBatchEbiddingErrorList = batchEbiddingErrorList.toArray(new EbiddingErrorDto[batchEbiddingErrorList.size()]);
                            processTransErrorList.add(addBatchEbiddingErrorList);

                            batchEbiddingErrorList.clear();

                        }
                        count++;
                    }

                    for (EbiddingErrorDto[] batchlist : processTransErrorList) {

                        Collection<EbiddingErrorDto> list = Arrays.asList(batchlist);
                        try {
                            CompletableFuture<int[][]> resultAddError = ebiddingErrorDao.batchAdd(list, list.size());
                            resultFutureAddError.add(resultAddError);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    for (Future<int[][]> futureError : resultFutureAddError) {
                        try {
                            futureError.get();
                        } catch (InterruptedException | ExecutionException e) {
                            e.printStackTrace();
                        }
                    }

                    resultFutureAddError.clear();
                }
                if (haveErrorRecordPost == true) {
                    totalRecordPost = 0;
                }
                EbiddingLogDto updateLog = new EbiddingLogDto();
                if (totalRecordError.intValue() == 0) {
                    String dateCodeUpdate = filename.substring(13, 21);
                    String dateStringUpdate = textEbiddingValidate.dateFormat(dateCodeUpdate);
                    updateLog.setTransdate(dateStringUpdate);
                    updateLog.setDoctype(ebiddingDocType);
                    updateLog.setFilename(filename);
                    updateLog.setTotamt(totalAmount);
                    updateLog.setTotrec(countTotalRecord);
                    updateLog.setPostrec(totalRecordPost);
                    updateLog.setErrrec(totalRecordError);
                    updateLog.setStatus("A");
                    String formatDateUpdateLog = new SimpleDateFormat("dd-MMM-yy HH:mm:ss").format(new Date());
                    updateLog.setUploadfinish(formatDateUpdateLog);
                    try {
                        fileStorageService.fileOutPass(filename, dateCode, ebiddingDocType, "Pass");
                        ebiddingLogDao.updateLogCode(updateLog, dateTime);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    String dateCodeUpdate = filename.substring(13, 21);
                    String dateStringUpdate = textEbiddingValidate.dateFormat(dateCodeUpdate);
                    updateLog.setTransdate(dateStringUpdate);
                    updateLog.setDoctype(ebiddingDocType);
                    updateLog.setFilename(filename);
                    updateLog.setTotamt(totalAmount);
                    updateLog.setTotrec(countTotalRecord);
                    updateLog.setPostrec(totalRecordPost);
                    updateLog.setErrrec(totalRecordError);
                    updateLog.setStatus("F");
                    String formatDateUpdateLog = new SimpleDateFormat("dd-MMM-yy HH:mm:ss").format(new Date());
                    updateLog.setUploadfinish(formatDateUpdateLog);
                    try {
                        ebiddingLogDao.updateLogCode(updateLog, dateTime);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                countLog = countLog + 1;
                if (countLog == countList) {
                    fileStorageService.deleteFileToProcessDir(filename);
                }
            }
            //Delete File Temp After Process
            try {
                Files.delete(Paths.get(fileProcess));
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (countList == 0) {
                int countLogError = ebiddingErrorDao.countError(filename);
                if (countLogError > 0) {
                    try {
                        ebiddingErrorDao.deleteItem(filename);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    fileStorageService.MoveFileToErrorDirDate(filename);
                    try {
                        ebiddingLogDao.updateLogStatusError(filename, newDateString);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    fileStorageService.MoveFiletoSuccessDirDate(filename);
                    try {
                        ebiddingLogDao.updateLogStatusPost(filename, newDateString);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private double convertStringToDouble(String number) {
        double money;
        try {
            return Double.parseDouble(number);
        } catch (NumberFormatException ex) {
            return (double) 0;
        } catch (NullPointerException ex) {
            return (double) 0;
        }
    }
}
