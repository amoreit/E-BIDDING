package com.newgfmisinterface.rpinterfaceebidding.job;

import com.newgfmisinterface.rpinterfaceebidding.dao.EbiddingErrorDao;
import com.newgfmisinterface.rpinterfaceebidding.dao.EbiddingLogDao;
import com.newgfmisinterface.rpinterfaceebidding.dao.EbiddingStatementDao;
import com.newgfmisinterface.rpinterfaceebidding.dto.EbiddingStatementDto;
import com.newgfmisinterface.rpinterfaceebidding.service.FileStorageService;
import com.newgfmisinterface.rpinterfaceebidding.service.ScanFilePathService;
import com.newgfmisinterface.rpinterfaceebidding.validate.TextEbiddingValidate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class ProcessJobSTM {
    private static Logger LOGGER = LoggerFactory.getLogger(ProcessJobSTM.class);

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private ScanFilePathService scanFilePath;

    @Autowired
    private EbiddingStatementDao ebiddingStatementDao;

    @Autowired
    private EbiddingLogDao ebiddingLogDao;

    @Autowired
    private EbiddingErrorDao ebiddingErrorDao;

    private TextEbiddingValidate textEbiddingValidate = new TextEbiddingValidate();

    public void jobRunner() {
        String uploadDate = new SimpleDateFormat("dd-MMM-yy HH:mm:ss").format(new Date());
        String projectCode = "GSTMTEBIDING";
        List<String> moveFile = scanFilePath.listFilesForProcess(new File(fileStorageService.getFileInputLocation().toString()), projectCode);
        int fileListForScan = 0;
        for (String moveFileToProcess : moveFile) {
            fileStorageService.moveFileToProcessDir(moveFileToProcess);
            fileListForScan++;
        }
        if (fileListForScan > 0) {
            List<String> scanFileName = scanFilePath.listFilesForProcess(new File(fileStorageService.getFileStorageLocation().toString()), projectCode);

            for (String filename : scanFileName) {
                File file = new File(fileStorageService.getFileStorageLocation().toString() + "/" + filename);
                String filenameFullPath = file.getAbsolutePath();
                List<String> fileByline = fileStorageService.readFilebyLine(filenameFullPath);
                ArrayList<Object> saveError = new ArrayList<>();
                int row = 0;
                for (String line : fileByline) {
                    row = row + 1;
                    if (row == 1) {
                        continue;
                    }
                    EbiddingStatementDto insertDataObj = textEbiddingValidate.splitlinetoObjStatement(line);

                    String dateForFile = textEbiddingValidate.dateFormat(insertDataObj.getTransdate());
                    int countInsert = ebiddingStatementDao.countInsert(filename);
                    try {
                        if(countInsert > 0){
                            ebiddingStatementDao.update(insertDataObj,uploadDate,filename);
                        }else {
                            ebiddingStatementDao.add(insertDataObj, dateForFile, uploadDate,filename);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try{
                        fileStorageService.MoveFileStatementSuccessDirDate(filename);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
